import 'package:saudi_dates/model/event.dart';

class Booking {
  String id;
  DateTime date;
  Event event;
  int seats;

  Booking({this.id, this.date, this.event, this.seats});

  factory Booking.fromJson(Map<String, dynamic> map) {
    return Booking(
        id: map['_id'] ?? "",
        seats: map['seats'] ?? "",
        date: DateTime.parse(map['date']),
        event: Event.fromJson(map['event']));
  }

  Map<String, dynamic> toJson() => {
        'id': id,
      };
}
