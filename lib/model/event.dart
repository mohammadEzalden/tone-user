import 'package:saudi_dates/model/seat.dart';

class Event {
  String id;
  String name;
  String image;
  String address;
  String description;
  int numberOfSeats;
  int bookedSeats;
  int numberOfAvailableSeats;
  double priceForOneSeat;
  String starName;
  String category;
  String currency;
  double lat;
  double lng;
  DateTime date;
  bool active;
  List<Seat> seats;

  Event(
      {this.id,
      this.active,
      this.name,
      this.image,
      this.numberOfSeats,
      this.priceForOneSeat,
      this.starName,
      this.bookedSeats,
      this.category,
      this.address,
      this.lng,
      this.currency,
      this.lat,
      this.seats,
      this.date,
      this.numberOfAvailableSeats,
      this.description});

  factory Event.fromJson(Map<String, dynamic> map) {
    List<Seat> _seats = [];
    if (map["seatsTypes"] != null)
      _seats.addAll((map['seatsTypes'] as List)
          .map<Seat>((json) => Seat.fromJson(json))
          .toList());
    return Event(
        id: map['_id'] ?? "",
        active: map['active'] ?? true,
        name: map['name'] ?? " ",
        bookedSeats: map['bookedSeats'] ?? 0,
        description: map['description'] ?? " ",
        image: map['imageUrl'] ?? " ",
        numberOfAvailableSeats: map['numberOfAvailableSeats'] ?? 0,
        starName: map['starName'] ?? "",
        address: map['address'] ?? "",
        numberOfSeats: map['numberOfSeats'] ?? "",
        category: map['category'] ?? "",
        currency: map['currency'] ?? "",
        lng: double.parse(map['lng'].toString()) ?? 0.0,
        lat: double.parse(map['lat'].toString()) ?? 0.0,
        date: DateTime.parse(map['date']),
        seats: _seats);
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'count': numberOfSeats,
    'price': priceForOneSeat,
    'image': image,
    'name': name,
    'size_gram': starName,
  };
}
