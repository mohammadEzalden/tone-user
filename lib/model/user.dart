class User {
  String id;
  String name;
  String phone;
  String email;
  String imageUrl;
  String customerId;

  User(
      {this.id,
      this.name,
      this.phone,
      this.email,
      this.imageUrl,
      this.customerId});

  factory User.fromJson(Map<String, dynamic> map) {
    return User(
      id: map['_id'] ?? "",
      name: map['name'] ?? "",
      phone: map['phone'] ?? "",
      email: map['email'] ?? "",
      imageUrl: map['imageUrl'] ?? "",
      customerId: map['customer_id'] ?? "",
    );
  }

  Map<String, dynamic> toJson() => {
        '_id': id ?? "",
        'name': name,
        'phone': phone,
        'email': email,
        'imageUrl': imageUrl ?? "",
        'customer_id': customerId ?? "",
      };
}
