import 'dart:ui';

import 'package:flutter/material.dart';

class myColors {
  static var purple100 = Color(0xFF904E95);
  static var purple85 = Color(0xFA904E95);
  static var purple69 = Color(0xFFB786B1);
  static var orange = Color(0xFFE96443);
  static var orange69 = Color(0xFFEE947F);
  static var deepPurple = Color(0xFF4C0451);
  static var gray = Color(0xFF707070);
  static var white = Colors.white;
}
