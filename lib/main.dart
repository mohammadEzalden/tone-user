import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:saudi_dates/const/myColors.dart';
import 'package:saudi_dates/screens/auth/login.dart';
import 'package:saudi_dates/screens/auth/signUp.dart';
import 'package:saudi_dates/screens/home/aboutUs.dart';
import 'package:saudi_dates/screens/home/bookingEvent.dart';
import 'package:saudi_dates/screens/home/eventDetails.dart';
import 'package:saudi_dates/screens/home/personalInfoPage.dart';
import 'package:saudi_dates/screens/home/profile.dart';
import 'package:saudi_dates/screens/home/settings.dart';
import 'package:saudi_dates/screens/home/termsAndConditions.dart';
import 'package:saudi_dates/splash.dart';
import 'package:saudi_dates/view_models/home_view_model.dart';
import 'package:saudi_dates/view_models/user_view_model.dart';

import 'home.dart';
import 'language/appLanguage.dart';
import 'language/appLocalizations.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();
  print("Handling a background message: ${message.messageId}");
}

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final AppLanguage appLanguage;
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  MyApp({this.appLanguage});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // Initialize FlutterFire:
      future: _initialization,
      builder: (context, snapshot) {
        // Check for errors
        if (snapshot.hasError) {
          return Text("Error");
        }

        // Once complete, show your application
        if (snapshot.connectionState == ConnectionState.done) {
          return MultiProvider(
            providers: [
              ChangeNotifierProvider(create: (context) => AppLanguage()),
              ChangeNotifierProvider(create: (context) => HomeViewModel()),
              ChangeNotifierProvider(create: (context) => UserViewModel()),
            ],
            child: Consumer<AppLanguage>(
              builder: (context, model, child) {
                return MaterialApp(
                  locale: model.appLocal,
                  localizationsDelegates: [
                    AppLocalizations.delegate,
                    GlobalMaterialLocalizations.delegate,
                    GlobalWidgetsLocalizations.delegate,
                    GlobalCupertinoLocalizations.delegate,
                  ],
                  supportedLocales: [
                    Locale("ar", 'AE'),
                    Locale("en", 'US'),
                    Locale("fr", 'FR'),
                    Locale("hi", 'IN'),
                    Locale("fa", 'IR'),
                  ],
                  theme: ThemeData(
                      primaryColor: myColors.purple100,
                      accentColor: myColors.purple100,
                      fontFamily: "Ubuntu"),
                  home: Splash(),
                  routes: {
                    '/LoginPage': (context) => LoginPage(),
                    '/SignUpPage': (context) => SignUpPage(),
                    '/HomePage': (context) => HomePage(),
                    '/ProfilePage': (context) => ProfilePage(),
                    '/EventDetailsPage': (context) => EventDetailsPage(),
                    '/BookingEventPage': (context) => BookingEventPage(),
                    '/PersonalInfoPage': (context) => PersonalInfoPage(),
                    '/SettingsPage': (context) => SettingsPage(),
                    '/AboutUsPage': (context) => AboutUsPage(),
                    '/TermsAndConditions': (context) => TermsAndConditions(),
                  },
                );
              },
            ),
          );
        }

        // Otherwise, show something whilst waiting for initialization to complete
        return MaterialApp(
          home: Scaffold(
              backgroundColor: myColors.purple100,
              body: Container(
                decoration: BoxDecoration(color: Colors.white),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        "assets/images/logo.png",
                        width: 100,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Image.asset(
                        "assets/images/tone.png",
                        width: 100,
                      ),
                    ],
                  ),
                ),
              )),
        );
      },
    );
  }
}
//AppLocalizations.of(context).translate("Next")

///flutter build apk --release
///flutter build apk --split-per-abi
///taskkill /F /IM dart.exe
///flutter pub run flutter_launcher_icons:main

//SH1
///cd C:\Program Files (x86)\Java\jdk1.8.0_144\bin
///keytool -list -v -alias androiddebugkey -keystore C:\Users\m-n-3\.android\debug.keystore
///keytool -list -v -keystore C:\Users\m-n-3\key.jks -alias key

//keytool -exportcert -alias androiddebugkey -keystore "C:\Users\m-n-3\.android\debug.keystore" | "C:\Users\m-n-3\Documents\sss\bin\openssl" sha1 -binary | "C:\Users\m-n-3\Downloads\sss\bin\openssl" base64
//keytool -exportcert -alias key -keystore C:\Users\m-n-3\key.jks | C:\Users\m-n-3\Documents\sss\bin\openssl sha1 -binary | "C:\Users\m-n-3\Documents\sss\bin\openssl" base64

//key store
///https://stackoverflow.com/questions/51168235/how-to-get-signed-apk-for-flutter-with-existing-app-keystore
///https://share-my-key.firebaseapp.com/__/auth/handler
