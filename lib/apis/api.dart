import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:saudi_dates/apis/auth/url.dart' as url;
import 'package:saudi_dates/model/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<String> uploadImage(File file) async {
  //create multipart request for POST or PATCH method
  var request = http.MultipartRequest("POST", Uri.parse(url.uploadImage()));
  var pic = await http.MultipartFile.fromPath("file", file.path);
  //add multipart to request
  request.files.add(pic);
  var response = await request.send();
  var responseData = await response.stream.toBytes();
  var responseString = String.fromCharCodes(responseData);
  return jsonDecode(responseString)['imageUrl'];
}

Future<User> editUser({User u, File image}) async {
  if (image != null) u.imageUrl = await uploadImage(image);
  Map<String, String> header = {'Content-Type': 'application/json'};
  Uri uri = Uri.parse(url.editUser(u.id));
  var response = await http.put(uri, headers: header, body: jsonEncode(u));
  if (response.statusCode != HttpStatus.ok) {
    return null;
  }
  final parsed = await jsonDecode(response.body);
  return User.fromJson(parsed);
}

void readNotification() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  Map<String, String> header = {'Content-Type': 'application/json'};
  Uri uri =
      Uri.parse(url.notification(jsonDecode(prefs.getString("user"))['_id']));
  var response = await http.put(uri, headers: header);
}
