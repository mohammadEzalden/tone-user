import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:crypto/crypto.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:saudi_dates/apis/auth/url.dart' as url;
import 'package:saudi_dates/const/myColors.dart';
import 'package:saudi_dates/const/widgets.dart';
import 'package:saudi_dates/view_models/home_view_model.dart';
import 'package:saudi_dates/view_models/user_view_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

Future<bool> checkConnect(BuildContext context) async {
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      print('connected');
      return true;
    }
  } on SocketException catch (_) {
    print('not connected');
    showToast("not connected", context);
    return false;
  }
  return false;
}

Future<void> login(
    {BuildContext context, String phone, String password}) async {
  if (await checkConnect(context) == false) return;
  FirebaseAuth auth = FirebaseAuth.instance;
  await FirebaseAuth.instance.verifyPhoneNumber(
    phoneNumber: phone,
    verificationCompleted: (PhoneAuthCredential credential) async {
      // ANDROID ONLY!

      // Sign the user in (or link) with the auto-generated credential
//      await auth.signInWithCredential(credential);
      try {
        UserCredential user = await auth.signInWithCredential(credential);
        print("successful");
        print(user);
        if (user != null) {
          await loginBackend(user, context, phone: phone);
          Navigator.popUntil(
            context,
            ModalRoute.withName('/'),
          );
          Navigator.pushNamed(context, '/HomePage');
        }
      } on FirebaseAuthException catch (e) {
        if (e.code == 'invalid-verification-code') {
          Navigator.pop(context);
          showToast("invalid verification code", context,
              gravity: ToastGravity.TOP);
        }
      } catch (e) {
        Navigator.pop(context);
        showToast("Error", context);
      }
    },
    verificationFailed: (FirebaseAuthException e) {
      if (e.code == 'invalid-phone-number') {
        print('The provided phone number is not valid.');
        showToast(e.message, context, gravity: ToastGravity.CENTER);
      }
      Navigator.pop(context);
    },
    codeSent: (String verificationId, int resendToken) async {
      final TextEditingController smsController = TextEditingController();
      final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

      String smsCode = 'xxxx';
      showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text("Verification code"),
          content: Form(
            key: _formKey,
            child: TextFormField(
              validator: (v) {
                if (v != null && v.length == 0) {
                  return "Please enter Verification code";
                }
                return null;
              },
              textInputAction: TextInputAction.done,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  border: UnderlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  hintText: "Verification code",
                  hintStyle: TextStyle(color: Colors.white)),
              controller: smsController,
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () async {
                if (_formKey.currentState.validate()) {
                  showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      });
                  PhoneAuthCredential credential = PhoneAuthProvider.credential(
                      verificationId: verificationId,
                      smsCode: smsController.text);

                  // Sign the user in (or link) with the credential

                  try {
                    UserCredential user =
                        await auth.signInWithCredential(credential);
                    print("successful");
                    print(user);
                    if (user != null) {
                      await loginBackend(user, context);
                      Navigator.popUntil(
                        context,
                        ModalRoute.withName('/'),
                      );
                      Navigator.pushNamed(context, '/HomePage');
                    }
                  } on FirebaseAuthException catch (e) {
                    if (e.code == 'invalid-verification-code') {
                      Navigator.pop(context);
                      showToast("Invalid verification code", context,
                          gravity: ToastGravity.TOP);
                    }
                  } catch (e) {
                    Navigator.pop(context);
                    showToast("Error", context);
                  }
                }
              },
              child: Text(
                "Ok",
                style: TextStyle(color: myColors.purple100),
              ),
            ),
            TextButton(
              onPressed: () {
                Navigator.pop(context);
                Navigator.pop(context);
              },
              child: Text(
                "Cancel",
                style: TextStyle(color: myColors.gray),
              ),
            ),
          ],
        ),
      );
    },
    timeout: const Duration(seconds: 120),
    codeAutoRetrievalTimeout: (String verificationId) {},
  );
}

Future<void> loginBackend(UserCredential userCredential, BuildContext context,
    {String phone}) async {
  FirebaseMessaging messaging = FirebaseMessaging.instance;
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  Map<String, String> header = {'Content-Type': 'application/json'};
  var response;
  Uri uri = Uri.parse(url.login());
  print(phone != null ? phone : userCredential.user.phoneNumber);
  response = await http.post(uri,
      body: jsonEncode({
        "id": userCredential.user.uid,
        "fcm_token": await messaging.getToken(),
        "name": userCredential.user.displayName,
        "email": userCredential.user.email,
        "phone": phone != null ? phone : userCredential.user.phoneNumber,
      }),
      headers: header);

  prefs.setString(
      "loginInfo",
      jsonEncode({
        "id": userCredential.user.uid,
        "fcm_token": await messaging.getToken(),
        "name": userCredential.user.displayName,
        "email": userCredential.user.email,
        "phone": phone != null ? phone : userCredential.user.phoneNumber,
      }));
  print(prefs.getString("loginInfo"));

  prefs.setString("user", jsonEncode(jsonDecode(response.body)['user']));
  prefs.setString("token", jsonDecode(response.body)['token']);
  uri = Uri.parse(url.auth());
  header['x-auth-token'] = jsonDecode(response.body)['token'];
  await http.get(uri, headers: header);
  Provider.of<HomeViewModel>(context, listen: false).myBooking = [];
  Provider.of<HomeViewModel>(context, listen: false).getNotifies();
  Provider.of<UserViewModel>(context, listen: false).getProfileInfo();
}

Future<UserCredential> signInWithGoogle({BuildContext context}) async {
  if (await checkConnect(context) == false) return null;

  final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();
  if (googleUser == null) return null;
  // Obtain the auth details from the request
  final GoogleSignInAuthentication googleAuth = await googleUser.authentication;

  // Create a new credential
  final credential = GoogleAuthProvider.credential(
    accessToken: googleAuth.accessToken,
    idToken: googleAuth.idToken,
  );

  UserCredential userCredential =
  await FirebaseAuth.instance.signInWithCredential(credential);
  if (userCredential == null) return null;
  await loginBackend(userCredential, context);
  return userCredential;
}

GoogleSignIn _googleSignIn = GoogleSignIn(
  // Optional clientId
  // clientId: '479882132969-9i9aqik3jfjd7qhci1nqf0bm2g71rm1u.apps.googleusercontent.com',
  scopes: <String>[
    'email',
    'https://www.googleapis.com/auth/contacts.readonly',
  ],
);

Future<void> _handleSignOut() => _googleSignIn.disconnect();

Future<bool> signInWithFacebook(BuildContext context) async {
  // Trigger the sign-in flow
  final LoginResult result = await FacebookAuth.instance.login();

  // Create a credential from the access token
  final FacebookAuthCredential facebookAuthCredential =
      FacebookAuthProvider.credential(result.accessToken.token);

  print(facebookAuthCredential);
  print(facebookAuthCredential.accessToken);
  UserCredential user =
      await FirebaseAuth.instance.signInWithCredential(facebookAuthCredential);
  if (user != null) {
    await loginBackend(user, context);
    Navigator.popUntil(
      context,
      ModalRoute.withName('/'),
    );
    Navigator.pushNamed(context, '/HomePage');
    return false;
  } else {
    Navigator.pop(context);
  }
  // Once signed in, return the UserCredential
  return true;
}

/// Generates a cryptographically secure random nonce, to be included in a
/// credential request.
String generateNonce([int length = 32]) {
  final charset =
      '0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._';
  final random = Random.secure();
  return List.generate(length, (_) => charset[random.nextInt(charset.length)])
      .join();
}

/// Returns the sha256 hash of [input] in hex notation.
String sha256ofString(String input) {
  final bytes = utf8.encode(input);
  final digest = sha256.convert(bytes);
  return digest.toString();
}

Future<UserCredential> signInWithApple(BuildContext context) async {
  // To prevent replay attacks with the credential returned from Apple, we
  // include a nonce in the credential request. When signing in with
  // Firebase, the nonce in the id token returned by Apple, is expected to
  // match the sha256 hash of `rawNonce`.
  final rawNonce = generateNonce();
  final nonce = sha256ofString(rawNonce);
  var redirectURL = "https://tone-ff574.firebaseapp.com/__/auth/handler";
  var clientID = "com.hykaia.ToneApp";
  // Request credential for the currently signed in Apple account.

  try {
    final appleCredential = await SignInWithApple.getAppleIDCredential(
        scopes: [
          AppleIDAuthorizationScopes.email,
          AppleIDAuthorizationScopes.fullName,
        ],
        nonce: nonce,
        webAuthenticationOptions: WebAuthenticationOptions(
            redirectUri: Uri.parse(redirectURL), clientId: clientID));
    // Create an `OAuthCredential` from the credential returned by Apple.
    final oauthCredential = OAuthProvider("apple.com").credential(
      idToken: appleCredential.identityToken,
      rawNonce: rawNonce,
    );

    // Sign in the user with Firebase. If the nonce we generated earlier does
    // not match the nonce in `appleCredential.identityToken`, sign in will fail.
    UserCredential userCredential =
        await FirebaseAuth.instance.signInWithCredential(oauthCredential);
    if (userCredential != null) {
      await loginBackend(userCredential, context);
      Navigator.popUntil(
        context,
        ModalRoute.withName('/'),
      );
      Navigator.pushNamed(context, '/HomePage');
      return null;
    } else {
      Navigator.pop(context);
    }
    return userCredential;
  } catch (e) {
    Navigator.pop(context);
  }
}
