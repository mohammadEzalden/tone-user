String get mainUrl {
  return 'https://toneap.com';
}

String login() {
  return '$mainUrl/auth/user/login';
}

String auth() {
  return '$mainUrl/auth/';
}

String allEvents() {
  return '$mainUrl/event/all/events';
}

String trendingEvent() {
  return '$mainUrl/event/booking/trending';
}

String getCategories() {
  return '$mainUrl/category';
}

String createBooking() {
  return '$mainUrl/booking';
}

String myEvent(String userId) {
  return '$mainUrl/booking/user/$userId';
}

String editUser(String userId) {
  return '$mainUrl/auth/user/$userId';
}

String uploadImage() {
  return '$mainUrl/upload';
}

String notification(String userId) {
  return '$mainUrl/notes/$userId';
}
