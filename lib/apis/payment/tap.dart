import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:saudi_dates/apis/auth/url.dart' as url;
import 'package:saudi_dates/const/widgets.dart';
import 'package:saudi_dates/view_models/home_view_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

Map<dynamic, dynamic> tapSDKResult;
String responseID = "";
String cartToken = "";
String sdkStatus = "";
String sdkErrorCode;
String sdkErrorMessage;
String sdkErrorDescription;
String _eventId;
String _amount;
BuildContext _context;
int _numberOfSeats;
int _index;
String _seatType;

Future<void> configureSDK(String eventId, String amount, BuildContext context,
    int numberOfSeats, int index, String seatType) async {
  _eventId = eventId;
  _amount = amount;
  _context = context;
  _index = index;
  _seatType = seatType;
  _numberOfSeats = numberOfSeats;
//  // configure app
//  booking();
  ///
//  configureApp();
//   sdk session configurations
//  setupSDKSession();
}

var configuration;

// configure app key and bundle-id (You must get those keys from tap)
//Future<void> configureApp() async {
//
//   configuration = PaymentSdkConfigurationDetails(
//      profileId: "profile id",
//      serverKey: "SGJNT62NKB-J22WG2ZGNW-RZHDKBZ6RH",
//      clientKey: "CHKMK7-PBNR6M-MVTM9T-6K669Q",
//      cartId: "cart id",
//      cartDescription: "cart desc",
//      merchantName: "merchant name",
//      screentTitle: "Pay with Card",
//      billingDetails: BillingDetails("billing name",
//          "billing email",
//          "billing phone",
//          "address line",
//          "country",
//          "city",
//          "state",
//          "zip code"),
//      locale: PaymentSdkLocale.EN, //PaymentSdkLocale.AR or PaymentSdkLocale.DEFAULT
//      amount: double.parse(_amount),
//      currencyCode: "sar",
//      merchantCountryCode: "sa");
//  configuration.showBillingInfo = true;
//  configuration.showShippingInfo = true;
//}
//setupSDKSession(){
//  FlutterPaytabsBridge.startCardPayment(configuration, (event) {
//    setState(() {
//      if (event["status"] == "success") {
//        // Handle transaction details here.
//        var transactionDetails = event["data"];
//        print(transactionDetails);
//      } else if (event["status"] == "error") {
//        // Handle error here.
//      } else if (event["status"] == "event") {
//        // Handle events here.
//      }
//    });
//  });
//}

// Platform messages are asynchronous, so we initialize in an async method.
//Future<void> setupSDKSession() async {
//  final SharedPreferences prefs = await SharedPreferences.getInstance();
//  try {
//
//    GoSellSdkFlutter.sessionConfigurations(
//        trxMode: TransactionMode.PURCHASE,
//        transactionCurrency: "sar",
//        amount: _amount,
//        customer: Customer(
//            customerId: jsonDecode(prefs.getString("user"))["customer_id"],
//            // customer id is important to retrieve cards saved for this customer
//            email:
//                jsonDecode(prefs.getString("user"))['email'] ?? "test@mail.com",
//            isdNumber: "9651",
//            number:
//                jsonDecode(prefs.getString("user"))['phone'] ?? "user.phone",
//            firstName:
//                jsonDecode(prefs.getString("user"))['name'] ?? "user.name",
//            middleName:
//                jsonDecode(prefs.getString("user"))['name'] ?? "user.name",
//            lastName:
//                jsonDecode(prefs.getString("user"))['name'] ?? "user.name",
//            metaData: null),
//        // Post URL
//        postURL: "https://tap.company",
//        // Payment description
//        paymentDescription: "paymentDescription",
//        // Payment Metadata
//
//        // Payment Reference
//        paymentReference: Reference(
//            acquirer: "acquirer",
//            gateway: "gateway",
//            payment: "payment",
//            track: "track",
//            transaction: "trans_910101",
//            order: "order_262625"),
//        // payment Descriptor
//        paymentStatementDescriptor: "paymentStatementDescriptor",
//        // Save Card Switch
//        isUserAllowedToSaveCard: true,
//        // Enable/Disable 3DSecure
//        isRequires3DSecure: true,
//        // Receipt SMS/Email
//        receipt: Receipt(false, false),
//        // Authorize Action [Capture - Void]
//        authorizeAction:
//            AuthorizeAction(type: AuthorizeActionType.CAPTURE, timeInHours: 10),
//        // Destinations
//        destinations: null,
//        // merchant id
//        // merchantID: "merchant.com.wi.saudidates",
//        // merchantID: "mbc55577@hotmail.com",
//        merchantID: "21592",
//        // Allowed cards
//        allowedCadTypes: CardType.ALL,
//        applePayMerchantID: "merchant.com.wi.saudidates",
////        applePayMerchantID: "21592",
//        allowsToSaveSameCardMoreThanOnce: false,
//        // pass the card holder name to the SDK
//        cardHolderName: "",
//        // disable changing the card holder name by the user
//        allowsToEditCardHolderName: true,
//        // select payments you need to show [Default is all, and you can choose between WEB-CARD-APPLEPAY ]
//        paymentType: PaymentType.ALL,
//        // Transaction mode
//        sdkMode: SDKMode.Sandbox);
//  } on PlatformException {
//    // platformVersion = 'Failed to get platform version.';
//  }
//
////  if (!mounted) return;
//
//  tapSDKResult = {};
//  startSDK();
//}

//Future<void> startSDK() async {
////    setState(() {
////      loaderController.start();
////    });
//
//
//  tapSDKResult = await GoSellSdkFlutter.startPaymentSDK;
////    loaderController.stopWhenFull();
//  print('>>>> ${tapSDKResult['sdk_result']}');
//  switch (tapSDKResult['sdk_result']) {
//    case "SUCCESS":
//      sdkStatus = "SUCCESS";
//      responseID = tapSDKResult['charge_id'];
//      if (tapSDKResult['sdk_result'] == "SUCCESS") {
//        booking();
//      }
//      handleSDKResult();
//      break;
//    case "FAILED":
//      sdkStatus = "FAILED";
//      showToast(sdkStatus, _context);
//      Navigator.pop(_context);
//      handleSDKResult();
//      break;
//    case "SDK_ERROR":
//      print('sdk error............');
////      showToast(tapSDKResult['sdk_error_message'], context);
//      print(tapSDKResult['sdk_error_code']);
//      print(tapSDKResult['sdk_error_message']);
//      print(tapSDKResult['sdk_error_description']);
//      print('sdk error............');
//      sdkErrorCode = tapSDKResult['sdk_error_code'].toString();
//      sdkErrorMessage = tapSDKResult['sdk_error_message'];
//      sdkErrorDescription = tapSDKResult['sdk_error_description'];
//      Navigator.pop(_context);
//      showToast(tapSDKResult['sdk_error_description'], _context);
//
//      break;
//
//    case "NOT_IMPLEMENTED":
////      Navigator.pop(context);
//
//      sdkStatus = "NOT_IMPLEMENTED";
////      showToast(sdkStatus, /context);
//
//      break;
//    case "CANCELLED":
//      sdkStatus = "CANCELLED";
//      Navigator.pop(_context);
//      showToast(sdkStatus, _context);
//      break;
//  }
//}

//Future<void> handleSDKResult() async {
//  switch (tapSDKResult['trx_mode']) {
//    case "CHARGE":
//      printSDKResult('Charge');
//      break;
//
//    case "AUTHORIZE":
//      printSDKResult('Authorize');
//      break;
//
//    case "SAVE_CARD":
//      printSDKResult('Save Card');
//
//      break;
//
//    case "TOKENIZE":
//      print('TOKENIZE token : ${tapSDKResult['token']}');
//      print('TOKENIZE token_currency  : ${tapSDKResult['token_currency']}');
//      print('TOKENIZE card_first_six : ${tapSDKResult['card_first_six']}');
//      print('TOKENIZE card_last_four : ${tapSDKResult['card_last_four']}');
//      print('TOKENIZE card_object  : ${tapSDKResult['card_object']}');
//      print('TOKENIZE card_exp_month : ${tapSDKResult['card_exp_month']}');
//      print('TOKENIZE card_exp_year    : ${tapSDKResult['card_exp_year']}');
//      print("____________________________sfdf");
//      responseID = tapSDKResult['token'];
////      bool res = await apiPayment.postOrderAfterPay(
////          id: responseID, cartToken: cartToken, context: context);
////      if (res) {
////        Navigator.popUntil(
////          context,
////          ModalRoute.withName('/'),
////        );
////        widget.pageController.jumpToPage(0);
////
////        gotoNextPage(
////            context,
////            OrderConfirmedScreen(
////              pageController: widget.pageController,
////            ));
////      }
//
//      break;
//  }
////  showToast(tapSDKResult['trx_mode'], context);
//}
//
//void printSDKResult(String trx_mode) async {
//  print('$trx_mode status                : ${tapSDKResult['status']}');
//  print('$trx_mode id               : ${tapSDKResult['charge_id']}');
//  print('$trx_mode  description        : ${tapSDKResult['description']}');
//  print('$trx_mode  message           : ${tapSDKResult['message']}');
//  print('$trx_mode  card_first_six : ${tapSDKResult['card_first_six']}');
//  print('$trx_mode  card_last_four   : ${tapSDKResult['card_last_four']}');
//  print('$trx_mode  card_object         : ${tapSDKResult['card_object']}');
//  print('$trx_mode  card_brand          : ${tapSDKResult['card_brand']}');
//  print('$trx_mode  card_exp_month  : ${tapSDKResult['card_exp_month']}');
//  print('$trx_mode  card_exp_year: ${tapSDKResult['card_exp_year']}');
//  print('$trx_mode  acquirer_id  : ${tapSDKResult['acquirer_id']}');
//  print(
//      '$trx_mode  acquirer_response_code : ${tapSDKResult['acquirer_response_code']}');
//  print(
//      '$trx_mode  acquirer_response_message: ${tapSDKResult['acquirer_response_message']}');
//  print('$trx_mode  source_id: ${tapSDKResult['source_id']}');
//  print('$trx_mode  source_channel     : ${tapSDKResult['source_channel']}');
//  print('$trx_mode  source_object      : ${tapSDKResult['source_object']}');
//  print(
//      '$trx_mode source_payment_type : ${tapSDKResult['source_payment_type']}');
//  responseID = tapSDKResult['charge_id'];
////  if (tapSDKResult['sdk_result'] == "SUCCESS") {
////    bool res = await apiPayment.postOrderAfterPay(
////        id: responseID, cartToken: cartToken, context: context);
////    if (res) {
////      Navigator.popUntil(
////        context,
////        ModalRoute.withName('/'),
////      );
////      widget.pageController.jumpToPage(0);
////
////      gotoNextPage(
////          context,
////          OrderConfirmedScreen(
////            pageController: widget.pageController,
////          ));
////    }
////  } else {
////    Navigator.pop(context);
////    showToast(tapSDKResult['sdk_result'], context);
////  }
//}


///
Future<void> booking() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  print(prefs.getString("user"));
  print(prefs.getString("token"));
  print(DateTime.now());
  Map<String, String> header = {
    'Content-Type': 'application/json',
    'x-auth-token': prefs.getString("token")
  };
  var response;
  Uri uri = Uri.parse(url.createBooking());
  response = await http.post(uri,
      body: jsonEncode({
        "charge_id": DateTime.now().toString(),
        "user_id": jsonDecode(prefs.getString("user"))['_id'],
        "event_id": _eventId,
        "numberOfSeats": _numberOfSeats,
        "seatType": _seatType,
        "index": _index,
      }),
      headers: header);
  final parsed = await jsonDecode(response.body);

  if (response.statusCode != HttpStatus.ok) {
    showToast(parsed['msg'], _context, gravity: ToastGravity.CENTER);
    Navigator.pop(_context);
    return;
  } else {
    Provider.of<HomeViewModel>(_context, listen: false).getMyBooking();
    Provider.of<HomeViewModel>(_context, listen: false).getNotifies();
    showToast("SUCCESS", _context, gravity: ToastGravity.CENTER);
    Navigator.popUntil(
      _context,
      ModalRoute.withName('/'),
    );
    Navigator.pushNamed(_context, '/HomePage');
  }
}
