import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:saudi_dates/apis/auth/url.dart' as url;
import 'package:saudi_dates/const/myColors.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'language/appLanguage.dart';

class Splash extends StatefulWidget {
  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<Splash> {
  FirebaseAuth auth = FirebaseAuth.instance;

  startTime() async {
    var _duration = Duration(milliseconds: 500);
    return Timer(_duration, navigationPage);
  }

  Future<bool> isActivate() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, String> header = {'Content-Type': 'application/json'};
    var response;
    Uri uri = Uri.parse(url.login());
    response = await http.post(uri,
        body: jsonEncode(jsonDecode(prefs.getString("loginInfo"))),
        headers: header);
    final parsed = await jsonDecode(response.body);
    print(parsed);
    //60eaa6e4d28d680022540289
    //60ddecbb159f3b0022225b63
    if (response.statusCode != HttpStatus.ok) {
      prefs.clear();
      return false;
    }
    if (parsed['user'] == null || parsed['user']['active'] == false) {
      prefs.clear();
      return false;
    }
    prefs.setString("user", jsonEncode(parsed['user']));
    prefs.setString("token", parsed['token']);
    uri = Uri.parse(url.auth());
    header['x-auth-token'] = parsed['token'];
    response = await http.get(uri, headers: header);
    return true;
  }

  void navigationPage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString("language") == null) {
      Locale myLocale = Localizations.localeOf(context);
      if (myLocale.languageCode == "ar")
        prefs.setString("language", "ar");
      else
        prefs.setString("language", "en");
    }
    Provider.of<AppLanguage>(context, listen: false)
        .changeLanguage(Locale(prefs.getString("language")));
    if (FirebaseAuth.instance.currentUser == null) {
      print('User is currently signed out!');
      Navigator.pushReplacementNamed(context, '/LoginPage');
    } else {
      if (!(await isActivate()))
        Navigator.pushReplacementNamed(context, '/LoginPage');
      else
        Navigator.pushReplacementNamed(context, '/HomePage');
    }
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: myColors.purple100,
        body: Container(
          decoration: BoxDecoration(color: Colors.white),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  "assets/images/logo.png",
                  width: width / 3,
                ),
                SizedBox(
                  height: 10,
                ),
                Image.asset(
                  "assets/images/tone.png",
                  width: width / 3,
                ),
              ],
            ),
          ),
        ));
  }
}
