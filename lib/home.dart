import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:saudi_dates/const/myColors.dart';
import 'package:saudi_dates/screens/home/eventBar.dart';
import 'package:saudi_dates/screens/home/homeBar.dart';
import 'package:saudi_dates/screens/home/notifications.dart';
import 'package:saudi_dates/screens/home/profile.dart';
import 'package:saudi_dates/view_models/home_view_model.dart';

import 'language/appLocalizations.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final PageController _pageController = PageController();
  int _selectedIndex = 0;
  FirebaseMessaging messaging = FirebaseMessaging.instance;

  @override
  void initState() {
    init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        children: [
          HomeBar(),
          EventBarPage(),
          NotificationPage(),
          ProfilePage(),
        ],
        onPageChanged: (p) {
          setState(() {
            _selectedIndex = p;
          });
        },
        controller: _pageController,
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [
                myColors.orange69,
                myColors.purple69,

//                    Color(0xffFCAF45),
//                        Color(0xffF58529),
              ]),
        ),
        child: BottomNavigationBar(
          showUnselectedLabels: true,
          unselectedItemColor: myColors.white,
          backgroundColor: Colors.transparent,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  "assets/icons/home.svg",
                  color: _selectedIndex == 0
                      ? myColors.deepPurple
                      : myColors.white,
                  height: 27,
                  width: 27,
                ),
                label: AppLocalizations.of(context).translate("Home")),
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  "assets/icons/bi_calendar-event.svg",
                  color: _selectedIndex == 1
                      ? myColors.deepPurple
                      : myColors.white,
                  height: 22,
                  width: 22,
                ),
                label: AppLocalizations.of(context).translate("Event")),
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  "assets/icons/clarity_bell-outline-badged.svg",
                  color: _selectedIndex == 2
                      ? myColors.deepPurple
                      : myColors.white,
                  height: 19,
                  width: 20,
                ),
                label: AppLocalizations.of(context).translate("Notification")),
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  "assets/icons/profile.svg",
                  color: _selectedIndex == 3
                      ? myColors.deepPurple
                      : myColors.white,
                  height: 20,
                  width: 20,
                ),
                label: AppLocalizations.of(context).translate("Profile")),
          ],
          onTap: _onTappedBar,
          selectedItemColor: myColors.deepPurple,
          currentIndex: _selectedIndex,
        ),
      ),
    );
  }

  void _onTappedBar(int value) {
    setState(() {
      _selectedIndex = value;
    });
    _pageController.jumpToPage(value);
  }

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  init() async {
    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
    if (Platform.isIOS) iOS_Permission();
    // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('app_icon');
    final IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings(
            onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    final MacOSInitializationSettings initializationSettingsMacOS =
        MacOSInitializationSettings();
    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS,
            macOS: initializationSettingsMacOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);

    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      print('Got a message whilst in the foreground!');
      print('Message data: ${message.data}');

      if (message.notification != null) {
        print('Message also contained a notification: ${message.notification}');
        const AndroidNotificationDetails androidPlatformChannelSpecifics =
            AndroidNotificationDetails('your channel id', 'your channel name',
                'your channel description',
                importance: Importance.max,
                priority: Priority.high,
                showWhen: false);
        const NotificationDetails platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);
        await flutterLocalNotificationsPlugin.show(
            0,
            message.notification.title,
            message.notification.body,
            platformChannelSpecifics,
            payload: 'shopper');
      }
      Provider.of<HomeViewModel>(context, listen: false).getNotifies();
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) async {
      Provider.of<HomeViewModel>(context, listen: false).getNotifies();
    });

    FirebaseMessaging.onBackgroundMessage((message) {
      Provider.of<HomeViewModel>(context, listen: false).getNotifies();
      return null;
    });
  }

  void iOS_Permission() async {
    final bool result = await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );
  }

  Future selectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: $payload');
    }
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
  }

  Future<dynamic> onSelectNotification(String payload) async {
    /*Do whatever you want to do on notification click. In this case, I'll show an alert dialog*/
  }
}
