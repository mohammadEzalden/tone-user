import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:saudi_dates/apis/api.dart' as api;
import 'package:saudi_dates/apis/auth/api_handler.dart' as http;
import 'package:saudi_dates/apis/auth/url.dart' as url;
import 'package:saudi_dates/model/booking.dart';
import 'package:saudi_dates/model/event.dart';
import 'package:saudi_dates/model/notify.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeViewModel extends ChangeNotifier {
  List<Event> events = [];
  List<Event> eventsByCategory = [];
  List<Event> eventsByFilter = [];
  List<Booking> myBooking = [];
  List<Event> trendingEvents = [];
  List<Notify> notifies = [];
  List<String> categories = [];
  bool eventsLoading;
  bool isEventsByFilter;
  bool myEventsLoading;

  HomeViewModel() {
    eventsLoading = true;
    myEventsLoading = true;
    isEventsByFilter = false;
//    getEvents();
    getCategories();
//    getMyBooking();
    getNotifies();
//    getTrendingEvents();
  }

  getNotifies() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    getEvents();
    await http.getApi(
        url: url.notification(jsonDecode(prefs.getString("user"))['_id']),
        result: (int code, dynamic data) {
          notifies = [];
          for (var item in data) {
            notifies.add(Notify.fromJson(item));
          }
          notifies = notifies.reversed.toList();
          getMyBooking();
          notifyListeners();
        });
    api.readNotification();
  }

  Future<void> getEvents() async {
    await http.getApi(
        url: url.allEvents(),
        result: (int code, dynamic data) {
          events = [];
          for (var item in data) {
            if (item['active']) events.add(Event.fromJson(item));
          }
          myEventsLoading = false;
          events = events.reversed.toList();

          notifyListeners();
        });
    return null;
  }

  getTrendingEvents() {
    http.getApi(
        url: url.trendingEvent(),
        result: (int code, dynamic data) {
          for (var item in data) {
            trendingEvents.add(Event.fromJson(item));
          }
          eventsLoading = false;
          notifyListeners();
        });
  }

  getCategories() {
    categories.add("All");
    http.getApi(
        url: url.getCategories(),
        result: (int code, dynamic data) {
          for (var item in data['categories']) {
            categories.add(item);
          }
          eventsLoading = false;
          notifyListeners();
        });
  }

  getMyBooking() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    print(jsonDecode(prefs.getString("user"))['_id']);
    await http.getApi(
        url: url.myEvent(jsonDecode(prefs.getString("user"))['_id']),
        result: (int code, dynamic data) {
          myBooking = [];
          for (var item in data) {
            myBooking.add(Booking.fromJson(item));
          }
          myEventsLoading = false;
          myBooking = myBooking.reversed.toList();

          notifyListeners();
        });
  }

  filterByCategory(String category) {
    if (category == "All")
      eventsByCategory.addAll(events);
    else {
      eventsByCategory = [];
      events.forEach((e) {
        if (e.category == category) eventsByCategory.add(e);
      });
    }
    isEventsByFilter = false;

    notifyListeners();
  }

  filterBySearch(String category, String query) {
    if (category == "All") {
      eventsByCategory = [];
      eventsByCategory.addAll(events);
    } else {
      eventsByCategory = [];
      events.forEach((e) {
        if (e.category == category) eventsByCategory.add(e);
      });
    }
    if (query != "") {
      eventsByCategory.removeWhere((element) => !element.name.contains(query));
    }

    isEventsByFilter = false;
    eventsByCategory = eventsByCategory.reversed.toList();

    notifyListeners();
  }

  filter(String category, String date, RangeValues rangeValues) {
    isEventsByFilter = true;
    eventsByFilter = [];
    eventsByFilter.addAll(events);
    if (category != null && category != "All") {
      eventsByFilter.removeWhere((e) => e.category != category);
      eventsByFilter.forEach((element) {
        print(element.name);
      });
    }
    try {
      DateTime temp = DateTime.parse(date);
      eventsByFilter.removeWhere((e) {
        if (e.date.difference(temp).inHours >= 24) {
          return true;
        }
        return false;
      });
    } catch (e) {}
    eventsByFilter.removeWhere((e) {
      if (e.priceForOneSeat < rangeValues.start ||
          e.priceForOneSeat > rangeValues.end) {
        return true;
      }
      return false;
    });
    eventsByFilter = eventsByFilter.reversed.toList();

    notifyListeners();
  }
}
