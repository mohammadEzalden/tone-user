import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:saudi_dates/apis/api.dart' as api;
import 'package:saudi_dates/const/widgets.dart';
import 'package:saudi_dates/language/appLocalizations.dart';
import 'package:saudi_dates/model/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserViewModel extends ChangeNotifier {
  bool loadingData;

  User user;

  UserViewModel() {
    getProfileInfo();
  }

  getProfileInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    user = User.fromJson(jsonDecode(prefs.getString("user")));
    notifyListeners();
  }

  editUser(User u, {File image, BuildContext context}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    u.id = jsonDecode(prefs.getString("user"))['_id'];
    User res = await api.editUser(u: u, image: image);
    if (res != null) {
      user = res;
      showToast(AppLocalizations.of(context).translate("Success"), context);
      prefs.setString("user", jsonEncode(res));
      Navigator.pop(context);
      Navigator.pop(context);
      notifyListeners();
    }
  }
}
