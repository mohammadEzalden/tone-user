import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:saudi_dates/const/myColors.dart';
import 'package:saudi_dates/customWidget/CustomClipper.dart';
import 'package:saudi_dates/language/appLocalizations.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final TextEditingController code = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 267,
              child: ClipPath(
                clipper: CustomClipPath(),
                child: Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          colors: [
                            myColors.orange,
                            myColors.purple100,

//                    Color(0xffFCAF45),
//                        Color(0xffF58529),
                          ]),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 33),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 60,
                              ),
                              SvgPicture.asset("assets/icons/tone.svg"),
                              SizedBox(
                                height: 20,
                              ),
                              Container(
                                color: Colors.white,
                                height: 2,
                                width: 64,
                              ),
                              SizedBox(
                                height: 45,
                              ),
                              Text(
                                AppLocalizations.of(context)
                                    .translate("signup"),
                                style: TextStyle(
                                    color: Colors.white, fontSize: 30),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )),
              ),
            ),
            SizedBox(
              height: 22,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 46),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      SvgPicture.asset(
                        "assets/icons/username.svg",
                      ),
                      SizedBox(
                        width: 13,
                      ),
                      Text(
                        AppLocalizations.of(context).translate("UserName"),
                        style:
                            TextStyle(color: myColors.purple100, fontSize: 16),
                      ),
                    ],
                  ),
                  Container(
                    width: 285,
                    child: TextFormField(
                      validator: (v) {
                        if (v != null && v.length == 0) {
                          return "يرجى ادخال رمز التفعيل";
                        }
                        return null;
                      },
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                          hintText: AppLocalizations.of(context)
                              .translate("UserName"),
                          border: InputBorder.none,
                          hintStyle: TextStyle(
                              color: myColors.purple100, fontSize: 12)),
                      controller: code,
                    ),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: myColors.purple100, width: 1))),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      SvgPicture.asset(
                        "assets/icons/email.svg",
                      ),
                      SizedBox(
                        width: 13,
                      ),
                      Text(
                        AppLocalizations.of(context).translate("Email"),
                        style:
                            TextStyle(color: myColors.purple100, fontSize: 16),
                      ),
                    ],
                  ),
                  Container(
                    width: 285,
                    child: TextFormField(
                      validator: (v) {
                        if (v != null && v.length == 0) {
                          return "يرجى ادخال رمز التفعيل";
                        }
                        return null;
                      },
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                          hintText: 'test@mail.com',
                          border: InputBorder.none,
                          hintStyle: TextStyle(
                              color: myColors.purple100, fontSize: 12)),
                      controller: code,
                    ),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: myColors.purple100, width: 1))),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      SvgPicture.asset(
                        "assets/icons/phone.svg",
                      ),
                      SizedBox(
                        width: 13,
                      ),
                      Text(
                        'Phone Number',
                        style:
                            TextStyle(color: myColors.purple100, fontSize: 16),
                      ),
                    ],
                  ),
                  Container(
                    width: 285,
                    child: TextFormField(
                      validator: (v) {
                        if (v != null && v.length == 0) {
                          return "يرجى ادخال رمز التفعيل";
                        }
                        return null;
                      },
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                          hintText: '011234567890',
                          border: InputBorder.none,
                          hintStyle: TextStyle(
                              color: myColors.purple100, fontSize: 12)),
                      controller: code,
                    ),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: myColors.purple100, width: 1))),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      SvgPicture.asset(
                        "assets/icons/password.svg",
                      ),
                      SizedBox(
                        width: 13,
                      ),
                      Text(
                        'Password',
                        style:
                            TextStyle(color: myColors.purple100, fontSize: 16),
                      ),
                    ],
                  ),
                  Container(
                    width: 285,
                    child: TextFormField(
                      validator: (v) {
                        if (v != null && v.length == 0) {
                          return "يرجى ادخال رمز التفعيل";
                        }
                        return null;
                      },
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                          hintText: '*********',
                          border: InputBorder.none,
                          hintStyle: TextStyle(
                              color: myColors.purple100, fontSize: 12)),
                      controller: code,
                    ),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: myColors.purple100, width: 1))),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      SvgPicture.asset(
                        "assets/icons/password.svg",
                      ),
                      SizedBox(
                        width: 13,
                      ),
                      Text(
                        'Confirm Password',
                        style:
                            TextStyle(color: myColors.purple100, fontSize: 16),
                      ),
                    ],
                  ),
                  Container(
                    width: 285,
                    child: TextFormField(
                      validator: (v) {
                        if (v != null && v.length == 0) {
                          return "يرجى ادخال رمز التفعيل";
                        }
                        return null;
                      },
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                          hintText: '*********',
                          border: InputBorder.none,
                          hintStyle: TextStyle(
                              color: myColors.purple100, fontSize: 12)),
                      controller: code,
                    ),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: myColors.purple100, width: 1))),
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  Row(
                    children: [
//                      SvgPicture.asset(
//                        "assets/icons/google.svg",
//                        height: 10,
//                        width: 15,
//                      ),
                      Icon(
                        Icons.check_box,
                        color: myColors.purple100,
                        size: 17,
                      ),
                      SizedBox(
                        width: 13,
                      ),
                      Text(
                        "You agree with our ",
                        style: TextStyle(color: myColors.purple100),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                          bottom: 3, // Space between underline and text
                        ),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                          color: myColors.orange,
                          width: 1.0, // Underline thickness
                        ))),
                        child: InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, "/TermsAndConditions");
                          },
                          child: Text(
                            "Terms & Conditions",
                            style: TextStyle(
                              color: myColors.orange,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 38,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
//                TextButton(
//                    onPressed: () {},
//                    child: Text(
//                      "SIGNUP",
//                      style: TextStyle(color: MyColors.white,fontSize: 18),
//                    ),
//                    style: ButtonStyle(
//
//                        padding: MaterialStateProperty.all(
//                            EdgeInsets.symmetric(
//                                horizontal: 42, vertical: 13)),
//                        backgroundColor:
//                        MaterialStateProperty.all(MyColors.white),
//                        shape: MaterialStateProperty.all<
//                            RoundedRectangleBorder>(
//                            RoundedRectangleBorder(
//                                borderRadius: BorderRadius.circular(18.0),
//                                side:
//                                BorderSide(color: Colors.grey[300]))))),
                Container(
                  height: 44,
                  width: 140,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: [
                          myColors.purple100,
                          myColors.orange,
                        ]),
                  ),
                  child: Center(
                      child: Text(
                    "SIGNUP",
                    style: TextStyle(color: myColors.white, fontSize: 16),
                  )),
                )
              ],
            ),
            SizedBox(
              height: 23,
            ),
          ],
        ),
      ),
    );
  }
}
