import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:saudi_dates/apis/auth/loginApi.dart' as api;
import 'package:saudi_dates/const/myColors.dart';
import 'package:saudi_dates/customWidget/CustomClipper.dart';
import 'package:saudi_dates/language/appLocalizations.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController phone = TextEditingController();
  final TextEditingController smsController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Stack(
                children: [
                  Container(
                    height: height / 1.7,
                    child: ClipPath(
                      clipper: CustomClipPath(),
                      child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                colors: [
                                  Color(0xffe96443),
                                  Color(0xff904e95),

//                    Color(0xffFCAF45),
//                        Color(0xffF58529),
                                ]),
                          ),
                          child: Padding(
                            padding: EdgeInsets.only(left: width / 9),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Theme(
                                  data: ThemeData(errorColor: Colors.white),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: height / 7,
                                      ),
                                      SvgPicture.asset("assets/icons/tone.svg"),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Container(
                                        color: Colors.white,
                                        height: 2,
                                        width: 64,
                                      ),
                                      SizedBox(
                                        height: height / 10,
                                      ),
                                      Text(
                                        AppLocalizations.of(context)
                                            .translate("Phone"),
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 16),
                                      ),
                                      Container(
                                        width: width / 1.7,
                                        child: TextFormField(
                                          validator: (v) {
                                            if (v != null && v.length == 0) {
                                              return AppLocalizations.of(
                                                  context)
                                                  .translate(
                                                  "Please enter phone number");
                                            }
                                            return null;
                                          },
                                          textInputAction: TextInputAction.done,
                                          keyboardType: TextInputType.phone,
                                          style: TextStyle(color: Colors.white),
                                          decoration: InputDecoration(
                                              hintText: '+966123456789',
                                              border: InputBorder.none,
                                              hintStyle: TextStyle(
                                                  color: Colors.white)),
                                          controller: phone,
                                        ),
                                        decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    color: Colors.white,
                                                    width: 1))),
                                      ),
                                      SizedBox(
                                        height: 27,
                                      ),
//                                      Text(
//                                        'Password',
//                                        style: TextStyle(
//                                            color: Colors.white, fontSize: 16),
//                                      ),
//                                      Container(
//                                        width: 285,
//                                        child: TextFormField(
//                                          style: TextStyle(color: Colors.white),
//                                          validator: (v) {
//                                            if (v != null && v.length == 0) {
//                                              return "Please enter password";
//                                            }
//                                            return null;
//                                          },
//
//                                          textInputAction: TextInputAction.done,
//                                          decoration: InputDecoration(
//                                            counterStyle: TextStyle(color: Colors.white),
//                                              border: InputBorder.none,
//                                              hintStyle: TextStyle(
//                                                  color: Colors.white)),
//                                          controller: smsController,
//                                        ),
//                                        decoration: BoxDecoration(
//                                            border: Border(
//                                                bottom: BorderSide(
//                                                    color: Colors.white,
//                                                    width: 1))),
//                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          )),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: height / 1.7 - 45,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          TextButton(
                              onPressed: () async {
                                if (_formKey.currentState.validate()) {
                                  buildShowDialog(context);

                                  api.login(
                                      context: context,
                                      phone: phone.text,
                                      password: smsController.text);
                                }
                              },
                              child: Text(
                                AppLocalizations.of(context).translate("Login"),
                                style: TextStyle(color: myColors.purple85),
                              ),
                              style: ButtonStyle(
                                  padding: MaterialStateProperty.all(
                                      EdgeInsets.symmetric(
                                          horizontal: 40, vertical: 13)),
                                  backgroundColor:
                                  MaterialStateProperty.all(myColors.white),
                                  shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius:
                                          BorderRadius.circular(18.0),
                                          side: BorderSide(
                                              color: Colors.grey[300]))))),

                          ///_________________________________________
//                          SizedBox(
//                            width: 20,
//                          ),
//                          TextButton(
//                              onPressed: () {
//                                Navigator.pushNamed(context, "/SignUpPage");
//                              },
//                              child: Text(
//                                "SignUp",
//                                style: TextStyle(color: myColors.purple69),
//                              ),
//                              style: ButtonStyle(
//                                  padding: MaterialStateProperty.all(
//                                      EdgeInsets.symmetric(
//                                          horizontal: 40, vertical: 13)),
//                                  backgroundColor:
//                                      MaterialStateProperty.all(myColors.white),
//                                  shape: MaterialStateProperty.all<
//                                          RoundedRectangleBorder>(
//                                      RoundedRectangleBorder(
//                                          borderRadius:
//                                              BorderRadius.circular(18.0),
//                                          side: BorderSide(
//                                              color: Colors.grey[300]))))),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 23,
              ),
              Text(AppLocalizations.of(context).translate("or login with")),
              SizedBox(
                height: 22,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () async {
                      buildShowDialog(context);
                      UserCredential user =
                      await api.signInWithGoogle(context: context);
                      if (user != null) {
                        Navigator.pop(context);
                        Navigator.pushReplacementNamed(context, "/HomePage");
                      } else
                        Navigator.pop(context);
                    },
                    child: Container(
                        height: 54,
                        width: 54,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: myColors.white,
                            boxShadow: [
                              BoxShadow(color: Colors.grey, spreadRadius: 0.5)
                            ]),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(16, 16, 16, 17.5),
                          child: SvgPicture.asset("assets/icons/google.svg"),
                        )),
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  InkWell(
                    onTap: () async {
                      buildShowDialog(context);
                      try {
                        api.signInWithFacebook(context);
                        Navigator.pop(context);
                      } catch (e) {}
                    },
                    child: Container(
                        height: 54,
                        width: 54,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: myColors.white,
                            boxShadow: [
                              BoxShadow(color: Colors.grey, spreadRadius: 0.5)
                            ]),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(22, 16, 22, 16),
                          child: SvgPicture.asset("assets/icons/facebook.svg"),
                        )),
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  InkWell(
                    onTap: () {
                      buildShowDialog(context);
                      try {
                        api.signInWithApple(context);
//                        Navigator.pop(context);
                      } catch (e) {}
                    },
                    child: Container(
                        height: 54,
                        width: 54,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: myColors.white,
                            boxShadow: [
                              BoxShadow(color: Colors.grey, spreadRadius: 0.5)
                            ]),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(1, 8, 1, 8),
                          child: SvgPicture.asset("assets/icons/apple.svg"),
                        )),
                  ),
                  SizedBox(
                    width: 16,
                  ),
                ],
              ),
              SizedBox(
                height: height / 9,
              ),
              Padding(
                padding: EdgeInsets.only(left: width / 13, right: width / 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                        child: Text(AppLocalizations.of(context)
                            .translate("By signing up, you agree with our "))),
                    Container(
                      padding: EdgeInsets.only(
                        bottom: 3, // Space between underline and text
                      ),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                        color: myColors.orange,
                        width: 1.0, // Underline thickness
                      ))),
                      child: InkWell(
                        onTap: () {
                          Navigator.pushNamed(context, "/TermsAndConditions");
                        },
                        child: Text(
                          AppLocalizations.of(context)
                              .translate("Terms & Conditions"),
                          style: TextStyle(
                            color: myColors.orange,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
//            RichText(
//              text: TextSpan(
//                text: "By signing up, you agree with our ",
//                style: TextStyle(color: Colors.black),
//                children: <TextSpan>[
//                  TextSpan(
//                    text: "Terms & Conditions"
//                        ,
//                    style: TextStyle(color: Color(0xFFE96443),decoration: TextDecoration.underline,),
//
//                  )
//                ]
//              ),
//            ),
              SizedBox(
                height: 23,
              ),
            ],
          ),
        ),
      ),
    );
  }

  buildShowDialog(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  Future<String> getPhone() async {
    String phone = "+19795555555";
    PhoneNumber number = await PhoneNumber.getRegionInfoFromPhoneNumber(phone);
    String formattedNumber = number.parseNumber();
    print(formattedNumber); // -> prints: '+1 979-555-5555'
    return formattedNumber;
  }
}
