import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:saudi_dates/const/imageViwer.dart';
import 'package:saudi_dates/const/myColors.dart';
import 'package:saudi_dates/customWidget/MoveCameraPage.dart';
import 'package:saudi_dates/customWidget/image_from_network.dart';
import 'package:saudi_dates/language/appLocalizations.dart';
import 'package:saudi_dates/model/event.dart';
import 'package:saudi_dates/screens/home/bookingEvent.dart';

class EventDetailsPage extends StatefulWidget {
  final Event e;

  EventDetailsPage({this.e});

  @override
  _EventDetailsPageState createState() => _EventDetailsPageState();
}

class _EventDetailsPageState extends State<EventDetailsPage> {
  final TextEditingController code = TextEditingController();
  String category = "all";
  GoogleMapController _controller;
  DateFormat fDay;

  DateFormat fDate;

  DateFormat fTime;

  @override
  Widget build(BuildContext context) {
    fDay = new DateFormat(
        "EEEE", AppLocalizations.of(context).locale.languageCode);
    fDate = new DateFormat(
        'yyyy-MM-dd', AppLocalizations.of(context).locale.languageCode);
    fTime =
        new DateFormat("jm", AppLocalizations.of(context).locale.languageCode);
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0), // here the desired height
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
              decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.topRight,
                colors: [
                  myColors.orange,
                  myColors.purple100,
                ]),
          )),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 8, right: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: height / 29,
              ),
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Row(
                  children: [
                    Icon(
                      Icons.arrow_back,
                      color: myColors.purple100,
                    ),
                    Text(
                      AppLocalizations.of(context).translate("Event Details"),
                      style: TextStyle(
                          fontSize: 18,
                          color: myColors.purple100,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: height / 29,
              ),
              InkWell(
                onTap: () {
//                  imageView(e.image);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ImageViewer(widget.e.image)));
                },
                child: Center(
                  child: Container(
                    height: height / 3.5,
                    width: width - 16,
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: ImageFromNetworkWidget(widget.e.image)),
                  ),
                ),
              ),
              SizedBox(
                height: height / 29,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 35, right: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.e.category,
                      style: TextStyle(fontSize: 18, color: myColors.purple100),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      fDay.format(widget.e.date) +
                          " " +
                          fDate.format(widget.e.date),
                      style: TextStyle(color: myColors.purple100),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      AppLocalizations.of(context).translate("at") +
                          " " +
                          fTime.format(widget.e.date),
                      style: TextStyle(color: myColors.purple100),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      AppLocalizations.of(context).translate("Available"),
                      style: TextStyle(fontSize: 18, color: myColors.purple100),
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          AppLocalizations.of(context).translate("All"),
                          style: TextStyle(color: myColors.purple100),
                        ),
                        Text(
                          widget.e.numberOfSeats.toString() +
                              " " +
                              AppLocalizations.of(context).translate("sit"),
                          style: TextStyle(color: myColors.purple100),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          AppLocalizations.of(context).translate("Booking"),
                          style: TextStyle(color: myColors.purple100),
                        ),
                        Text(
                          (widget.e.bookedSeats).toString() +
                              " " +
                              AppLocalizations.of(context).translate("sit"),
                          style: TextStyle(color: myColors.purple100),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          AppLocalizations.of(context).translate("Available"),
                          style: TextStyle(color: myColors.purple100),
                        ),
                        Text(
                          widget.e.numberOfAvailableSeats.toString() +
                              " " +
                              AppLocalizations.of(context).translate("sit"),
                          style: TextStyle(color: myColors.purple100),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 21,
                    ),
                    Text(
                      AppLocalizations.of(context).translate("Description"),
                      style: TextStyle(fontSize: 18, color: myColors.purple100),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      widget.e.description,
                      style: TextStyle(color: myColors.purple100),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      AppLocalizations.of(context).translate("Location"),
                      style: TextStyle(fontSize: 18, color: myColors.purple100),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                  ],
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(color: myColors.purple100, spreadRadius: 1),
                  ],
                ),
                height: 147,
                width: width - 16,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: GoogleMap(
                    onTap: (l) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  MoveCamera(
                                      LatLng(widget.e.lat, widget.e.lng))));
                    },
                    mapType: MapType.normal,
                    initialCameraPosition: CameraPosition(
                      target: LatLng(widget.e.lat, widget.e.lng),
                      zoom: 14.4746,
                    ),
                    onMapCreated: (GoogleMapController controller) {
                      _controller = controller;
                    },
                    zoomControlsEnabled: false,
                  ),
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                width: width,
                height: 38,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Color(0xff904e95),
                        Color(0xffe96443),
                      ],
                    ),
                    borderRadius: new BorderRadius.all(Radius.circular(8.0))),
                child: TextButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                BookingEventPage(e: widget.e)));
                  },
                  child: Text(
                    AppLocalizations.of(context).translate("Booking Ticket"),
                    style: TextStyle(color: myColors.white, fontSize: 18),
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
