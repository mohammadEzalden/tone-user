import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:saudi_dates/const/myColors.dart';
import 'package:saudi_dates/language/appLocalizations.dart';
import 'package:saudi_dates/view_models/home_view_model.dart';

class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  final TextEditingController code = TextEditingController();
  String category = "all";
  DateFormat fDay;

  @override
  Widget build(BuildContext context) {
    fDay =
        new DateFormat("EEEE", AppLocalizations.of(context).locale.languageCode)
            .add_jm()
            .add_yMMMMd();

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0), // here the desired height
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
              decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.topRight,
                colors: [
                  myColors.orange,
                  myColors.purple100,
                ]),
          )),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 8, right: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: height / 29,
              ),
              Text(
                AppLocalizations.of(context).translate("Notification"),
                style: TextStyle(
                    fontSize: 18,
                    color: myColors.purple100,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: height / 29,
              ),
              Consumer<HomeViewModel>(builder: (context, home, child) {
                if (home.notifies.isEmpty)
                  return Padding(
                    padding: EdgeInsets.only(top: height / 4),
                    child: Center(
                        child: Text(
                      AppLocalizations.of(context)
                          .translate("There is no Notification"),
                      style: TextStyle(color: Colors.grey, fontSize: 20),
                    )),
                  );
                return Column(
                  children: home.notifies
                      .map((e) => Padding(
                    padding: const EdgeInsets.only(bottom: 9),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: myColors.purple69),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(11.0),
                                child: Column(
                                  children: [
                                    Text(
                                      e.message,
                                      style: TextStyle(
                                          color: myColors.purple100,
                                          fontSize: 20),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Divider(),
                                    Text(fDay.format(e.date))
                                  ],
                                ),
                              ),
                            ),
                          ))
                      .toList(),
                );
              }),
            ],
          ),
        ),
      ),
    );
  }

}
