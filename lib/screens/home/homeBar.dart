import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:saudi_dates/const/myColors.dart';
import 'package:saudi_dates/customWidget/image_from_network.dart';
import 'package:saudi_dates/language/appLocalizations.dart';
import 'package:saudi_dates/screens/home/eventDetails.dart';
import 'package:saudi_dates/view_models/home_view_model.dart';

class HomeBar extends StatefulWidget {
  @override
  _HomeBarState createState() => _HomeBarState();
}

class _HomeBarState extends State<HomeBar> {
  final TextEditingController query = TextEditingController();
  final TextEditingController dateCont = TextEditingController();
  final startTextController = TextEditingController();
  final endTextController = TextEditingController();
  String category;
  String filterCategory;
  RangeValues _currentRangeValues = const RangeValues(1, 1000);
  DateFormat fDay;
  DateFormat fDate;

  DateFormat fTime;
  DateFormat f;

  @override
  void initState() {
    category = "All";
    startTextController.text = _currentRangeValues.start.toStringAsFixed(0);
    endTextController.text = _currentRangeValues.end.toStringAsFixed(0);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    fDay = new DateFormat(
        "EEEE", AppLocalizations.of(context).locale.languageCode);
    fDate = new DateFormat(
        'yyyy-MM-dd', AppLocalizations.of(context).locale.languageCode);
    fTime =
        new DateFormat("jm", AppLocalizations.of(context).locale.languageCode);
    f = DateFormat(
        'yyyy-MM-dd hh:mm', AppLocalizations.of(context).locale.languageCode);
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(0.0), // here the desired height
          child: AppBar(
            automaticallyImplyLeading: false,
            flexibleSpace: Container(
                decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.topRight,
                  colors: [
                    myColors.orange,
                    myColors.purple100,
                  ]),
            )),
          ),
        ),
        body: RefreshIndicator(
          onRefresh: refreshList,
          child: SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 8, right: 8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: height / 29,
                      ),
                      SvgPicture.asset(
                        "assets/icons/tone.svg",
                        color: myColors.purple100,
                      ),
                      SizedBox(
                        height: height / 29,
                      ),
                      Container(
                        child: Row(
                          children: [
                            Expanded(
                              child: TextFormField(
                                textInputAction: TextInputAction.search,
                                decoration: InputDecoration(
                                    prefixIcon: IconButton(
                                      icon: SvgPicture.asset(
                                        "assets/icons/search.svg",
                                        height: 18,
                                        width: 18,
                                      ),
//                                onPressed: () {},
                                    ),
                                    hintText: AppLocalizations.of(context)
                                        .translate("Search"),
                                    border: InputBorder.none,
                                    hintStyle:
                                        TextStyle(color: myColors.purple69)),
                                controller: query,
                                onChanged: (v) {
                                  Provider.of<HomeViewModel>(context,
                                          listen: false)
                                      .filterBySearch(category, v);
                                },
                              ),
                            ),
                            IconButton(
                              icon: SvgPicture.asset(
                                "assets/icons/filter.svg",
                                height: 19,
                                width: 20,
                              ),
                              onPressed: () {
                                _showPicker(context, width, height);
                              },
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30),
                            border: Border.all(color: myColors.gray)),
                      ),
                    ],
                  ),
                ),
//            TrendingEvents(),
                Padding(
                  padding: EdgeInsets.only(left: 8, right: 8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: height / 29,
                      ),
                      Text(
                        AppLocalizations.of(context).translate("by category"),
                        style:
                            TextStyle(fontSize: 18, color: myColors.purple100),
                      ),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Consumer<HomeViewModel>(
                                builder: (context, home, child) {
                                  if (home.eventsLoading)
                                    return Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  return Wrap(
                                    children: getServicesList(home.categories),
                                  );
                                }),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Text(
                        AppLocalizations.of(context)
                            .translate("Recommended for you"),
                        style:
                        TextStyle(fontSize: 18, color: myColors.purple100),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Consumer<HomeViewModel>(builder: (context, home, child) {
                        if (home.eventsLoading)
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        if (home.isEventsByFilter)
                          return Column(
                            children: home.eventsByFilter
                                .map((e) => InkWell(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    EventDetailsPage(
                                                      e: e,
                                                    )));
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            top: 7),
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment
                                              .start,

                                          children: [
                                            Container(
                                              height: height / 9,
                                              width: width / 2.1,
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                BorderRadius.circular(10),
                                                color: myColors.purple100,
                                              ),
                                              child: ClipRRect(
                                                borderRadius:
                                                BorderRadius.circular(8.0),
                                                child: ImageFromNetworkWidget(
                                                    e.image),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 16,
                                            ),
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    e.name,
                                                    style: TextStyle(
                                                        fontSize: 15,
                                                        fontWeight:
                                                        FontWeight.bold,
                                                        color:
                                                        myColors.purple100),
                                                  ),
                                                  SizedBox(
                                                    height: 8,
                                                  ),
                                                  Text(
                                                    fDay.format(e.date) +
                                                        "\n" +
                                                        fDate.format(e.date),
                                                    style: TextStyle(
                                                        color:
                                                        myColors.purple100),
                                                  ),
                                                  Text(
                                                    AppLocalizations.of(context)
                                                        .translate("at") +
                                                        " " +
                                                        fTime.format(e.date),
                                                    style: TextStyle(
                                                        color:
                                                        myColors.purple100),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ))
                                .toList(),
                          );
                        else if (category != "All" || query.text.isNotEmpty)
                          return Column(
                            children: home.eventsByCategory
                                .map((e) => InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            EventDetailsPage(
                                              e: e,
                                            )));
                              },
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    top: 7),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,

                                  children: [
                                    Container(
                                      height: height / 9,
                                      width: width / 2.1,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                        BorderRadius.circular(10),
                                        color: myColors.purple100,
                                      ),
                                      child: ClipRRect(
                                        borderRadius:
                                        BorderRadius.circular(8.0),
                                        child: ImageFromNetworkWidget(
                                                    e.image),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 16,
                                            ),
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    e.name,
                                                    style: TextStyle(
                                                        fontSize: 15,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color:
                                                            myColors.purple100),
                                                  ),
                                                  SizedBox(
                                                    height: 8,
                                                  ),
                                                  Text(
                                                    fDay.format(e.date) +
                                                        "\n" +
                                                        fDate.format(e.date),
                                                    style: TextStyle(
                                                        color:
                                                            myColors.purple100),
                                                  ),
                                                  Text(
                                                    AppLocalizations.of(context)
                                                            .translate("at") +
                                                        " " +
                                                        fTime.format(e.date),
                                                    style: TextStyle(
                                                        color:
                                                        myColors.purple100),
                                                  ),
                                                ],
                                              ),
                                            )
                                  ],
                                ),
                              ),
                            ))
                                .toList(),
                          );

                        return Column(
                          children: home.events
                              .map((e) =>
                              InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              EventDetailsPage(
                                                e: e,
                                              )));
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      top: 7),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .start,
                                    children: [
                                      Container(
                                        height: height / 9,
                                        width: width / 2.1,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                          BorderRadius.circular(10),
                                          color: myColors.purple100,
                                        ),
                                        child: ClipRRect(
                                          borderRadius:
                                          BorderRadius.circular(8.0),
                                          child: ImageFromNetworkWidget(
                                              e.image),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 16,
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                          MainAxisAlignment.start,
                                          children: [
                                            Text(
                                              e.name,
                                              style: TextStyle(
                                                  fontSize: 15,
                                                  fontWeight:
                                                  FontWeight.bold,
                                                  color:
                                                  myColors.purple100),
                                            ),
                                            SizedBox(
                                              height: 9,
                                            ),
                                            Text(
                                              fDay.format(e.date) +
                                                  "\n" +
                                                  fDate.format(e.date),
                                              style: TextStyle(
                                                  color:
                                                  myColors.purple100),
                                            ),
                                            Text(
                                              AppLocalizations.of(context)
                                                  .translate("at") +
                                                  " " +
                                                  fTime.format(e.date),
                                              style: TextStyle(
                                                  color:
                                                  myColors.purple100),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ))
                              .toList(),
                        );
                      }),
                      SizedBox(
                        height: 100,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }

  Future<void> refreshList() async {
    await Provider.of<HomeViewModel>(context, listen: false).getEvents();
  }

  List<Widget> getServicesList(List<String> categories) {
    List<Widget> temp = [];
    temp = categories
        .map((e) =>
        Padding(
          padding: const EdgeInsets.only(right: 14),
          child: ChoiceChip(
            onSelected: (b) {
              setState(() {
                category = e;
                Provider.of<HomeViewModel>(context, listen: false)
                    .filterByCategory(category);
              });
            },
            shape: StadiumBorder(side: BorderSide(color: Colors.grey)),
            selected: category == e,
            selectedColor: myColors.purple100,
            backgroundColor: myColors.white,
            padding: EdgeInsets.only(left: 8, right: 8),
            label: Text(
              e,
              style: TextStyle(
                  color: category == e ? Colors.white : myColors.purple100),
            ),
          ),
        ))
        .toList();
    return temp;
  }

  void _showPicker(context, double width, double height) {
    FocusScope.of(context).requestFocus(FocusNode());
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30), topRight: Radius.circular(30))),
        builder: (BuildContext bc) {
          return Container(
            height: 550,
            child: Padding(
              padding: EdgeInsets.only(top: 20, left: 12, right: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppLocalizations.of(context).translate("Filter"),
                    style: TextStyle(fontSize: 25, color: myColors.purple100),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        AppLocalizations.of(context).translate("Type of event"),
                        style: TextStyle(color: myColors.purple100),
                      ),
                      Container(
                        width: width / 2.3,
                        child: Consumer<HomeViewModel>(
                          builder: (context, home, child) {
                            return DropdownButtonFormField(
                              decoration: InputDecoration(
                                  errorStyle: TextStyle(height: 0.05),
                                  contentPadding: EdgeInsets.only(
                                      left: 20, top: 7, bottom: 7, right: 24),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        width: 1.0,
                                        style: BorderStyle.solid,
                                        color: myColors.purple100),
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(30.0)),
                                  ),
                                  filled: true,
                                  hintStyle:
                                  TextStyle(color: myColors.purple100),
                                  hintText: AppLocalizations.of(context)
                                      .translate("Type of event"),
                                  fillColor: Colors.white),
                              value: filterCategory,
                              onChanged: (String Value) {
                                setState(() {
                                  filterCategory = Value;
                                });
                              },
                              items: home.categories
                                  .map((cityTitle) =>
                                  DropdownMenuItem(
                                      value: cityTitle,
                                      child: Text(
                                        "$cityTitle",
                                        style: TextStyle(
                                            color: myColors.purple100),
                                      )))
                                  .toList(),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    AppLocalizations.of(context).translate("Date"),
                    style: TextStyle(color: myColors.purple100),
                  ),
                  SizedBox(
                    height: 7,
                  ),
                  Container(
                    width: width - 16,
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: TextFormField(
                      readOnly: true,
                      validator: (v) {
                        if (v != null && v.length == 0) {
                          return AppLocalizations.of(context)
                              .translate("Please Enter Date");
                        }
                        return null;
                      },
                      onTap: () {
                        DatePicker.showDateTimePicker(context,
                            showTitleActions: true,
                            minTime: DateTime.now(),
                            maxTime: DateTime(2101, 6, 7),
                            onChanged: (date) {
                              print('change $date');
                            },
                            onConfirm: (date) {
                              print('confirm $date');
                              dateCont.text = f.format(date);
                            },
                            currentTime:
                            DateTime.now().subtract(Duration(days: 30)),
                            locale: LocaleType.en);
                      },
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                          errorStyle: TextStyle(height: 0.05),
                          hintText:
                          AppLocalizations.of(context).translate("Date"),
                          border: InputBorder.none,
                          suffixIcon: Icon(
                            Icons.date_range,
                            color: myColors.purple100,
                          ),
                          hintStyle: TextStyle(color: myColors.purple69)),
                      controller: dateCont,
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        border: Border.all(color: myColors.purple100)),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    AppLocalizations.of(context).translate("Price"),
                    style: TextStyle(color: myColors.purple100),
                  ),
                  SizedBox(
                    height: 7,
                  ),
                  StatefulBuilder(builder: (thisLowerContext, innerSetState) {
                    return Column(
                      children: [
                        RangeSlider(
                          values: _currentRangeValues,
                          min: 1,
                          max: 1000,
                          divisions: 1000,
                          activeColor: myColors.purple100,
                          inactiveColor: Colors.grey,
                          labels: RangeLabels(
                            _currentRangeValues.start.round().toString(),
                            _currentRangeValues.end.round().toString(),
                          ),
                          onChanged: (RangeValues values) {
                            innerSetState(() {
                              _currentRangeValues = values;
                              startTextController.text =
                                  _currentRangeValues.start.toStringAsFixed(0);
                              endTextController.text =
                                  _currentRangeValues.end.toStringAsFixed(0);
                            });
                          },
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Container(
                          child: Row(
                            children: [
                              Container(
                                  width: 80,
                                  child: Container(
                                    padding: EdgeInsets.all(8),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(2)),
                                        border: Border.all(
                                            color: myColors.purple100,
                                            width: 1)),
                                    child: TextFormField(
                                      onChanged: (String value) {
                                        innerSetState(() {
                                          var start =
                                              double.tryParse(value) ?? 0.0;
                                          print(start);
                                          if (value != "" &&
                                              start < _currentRangeValues.end) {
                                            startTextController.text =
                                                start.toStringAsFixed(0);
                                            _currentRangeValues = RangeValues(
                                                start, _currentRangeValues.end);
                                          } else
                                            startTextController.text =
                                                _currentRangeValues.start
                                                    .toStringAsFixed(0);
                                          startTextController.selection =
                                              TextSelection.fromPosition(
                                                  TextPosition(
                                                      offset:
                                                      startTextController
                                                          .text.length));
                                        });
                                      },
                                      controller: startTextController,
                                      keyboardType: TextInputType.number,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: myColors.purple100,
                                          fontWeight: FontWeight.bold),
                                      decoration: InputDecoration.collapsed(
                                        hintText: "",
                                      ),
                                    ),
                                  )),
                              Expanded(child: Container()),
                              Container(
                                  width: 80,
                                  child: Container(
                                    padding: EdgeInsets.all(8),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(2)),
                                        border: Border.all(
                                            color: myColors.purple100,
                                            width: 1)),
                                    child: TextFormField(
                                      onChanged: (String value) {
                                        innerSetState(() {
                                          var end =
                                              double.tryParse(value) ?? 1000.0;

                                          if (value != "" &&
                                              end > _currentRangeValues.start &&
                                              end < 1000) {
                                            endTextController.text =
                                                end.toStringAsFixed(0);
                                            _currentRangeValues = RangeValues(
                                                _currentRangeValues.start, end);
                                          } else
                                            endTextController.text =
                                                _currentRangeValues.end
                                                    .toStringAsFixed(0);
                                          endTextController.selection =
                                              TextSelection.fromPosition(
                                                  TextPosition(
                                                      offset: endTextController
                                                          .text.length));
                                        });
                                      },
                                      style: TextStyle(
                                          color: myColors.purple100,
                                          fontWeight: FontWeight.bold),
                                      keyboardType:
                                      TextInputType.numberWithOptions(
                                          decimal: true),
                                      textAlign: TextAlign.center,
                                      controller: endTextController,
                                      decoration: InputDecoration.collapsed(
                                        hintText: "",
                                      ),
                                    ),
                                  )),
                            ],
                          ),
                        ),
                      ],
                    );
                  }),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      RawMaterialButton(
                        fillColor: myColors.purple100,
                        splashColor: myColors.purple100,
                        shape: StadiumBorder(
                            side: BorderSide(color: myColors.purple100)),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: width / 13, vertical: 15.0),
                          child: Text(
                            AppLocalizations.of(context).translate("Filter"),
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        onPressed: () {
                          category = filterCategory;
                          Provider.of<HomeViewModel>(context, listen: false)
                              .filter(filterCategory, dateCont.text,
                              _currentRangeValues);
                          Navigator.pop(context);
                          FocusScope.of(context).requestFocus(FocusNode());
                        },
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 30,
                  ),
                ],
              ),
            ),
          );
        });
  }
}
