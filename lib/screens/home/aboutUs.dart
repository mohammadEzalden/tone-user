import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:saudi_dates/const/myColors.dart';

class AboutUsPage extends StatefulWidget {
  @override
  _AboutUsPageState createState() => _AboutUsPageState();
}

class _AboutUsPageState extends State<AboutUsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0), // here the desired height
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
              decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.topRight,
                colors: [
                  myColors.orange,
                  myColors.purple100,
                ]),
          )),
        ),
      ),
      body: Directionality(
        textDirection: TextDirection.rtl,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 29,
              ),
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Icon(
                      Icons.arrow_forward,
                      color: myColors.purple100,
                    ),
                  ],
                ),
              ),

              SizedBox(
                height: 10,
              ),
              Text(
                "مؤسسة تون بلس للفعاليات الترفيهية",
                style: TextStyle(fontSize: 21),
              ),
              SizedBox(
                height: 21,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "السجل التجاري: ",
                    style: TextStyle(color: myColors.purple100),
                  ),
                  Text(
                    "١٠١٠٧٢٢١٤٦",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              SizedBox(
                height: 21,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "الموقع:",
                    style: TextStyle(color: myColors.purple100),
                  ),
                  Text(
                    "مدينة الرياض",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              Text(
                "٦٣٧٦، التخصصي - منطقة الرياض",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text(
                "وحدة رقم: ٦٣٨٤",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text(
                "الرياض١٢٣١٢-٣٤٨٠",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 21,
              ),
              Text(
                "رقم التواصل والايميل:",
                style: TextStyle(color: myColors.purple100),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text("رقم التواصل:"),
                  Text(
                    "0553554481",
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.blue),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text("الايميل:"),
                  Text(
                    "info@toneap.com",
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.blue),
                  ),
                ],
              ),
              Expanded(
                child: SizedBox(),
              ),
              Text(
                "وسائل الدفع المتاحة",
                style: TextStyle(color: myColors.purple100),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    height: 35,
                    width: 35,
                    child: Image.asset("assets/images/stcpay.png"),
                  ),
                  Container(
                    height: 47,
                    width: 47,
                    child: Image.asset("assets/images/visa.png"),
                  ),
                  Container(
                    height: 47,
                    width: 47,
                    child: Image.asset("assets/images/mastercard.png"),
                  ),
                  SvgPicture.asset(
                    "assets/images/mada.svg",
                    fit: BoxFit.cover,
                    height: 47,
                    width: 47,
                  ),
                  Platform.isIOS || true
                      ? SvgPicture.asset(
                          "assets/images/Apple_Pay_Mark.svg",
                          height: 30,
                          width: 30,
                        )
                      : Container()
                ],
              ),
              SizedBox(
                height: 15,
              )
            ],
          ),
        ),
      ),
    );
  }
}
