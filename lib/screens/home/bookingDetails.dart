import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:saudi_dates/const/imageViwer.dart';
import 'package:saudi_dates/const/myColors.dart';
import 'package:saudi_dates/customWidget/MoveCameraPage.dart';
import 'package:saudi_dates/customWidget/image_from_network.dart';
import 'package:saudi_dates/language/appLocalizations.dart';
import 'package:saudi_dates/model/booking.dart';
class BookingDetailsPage extends StatefulWidget {
  final Booking b;

  BookingDetailsPage({this.b});

  @override
  _BookingDetailsPageState createState() => _BookingDetailsPageState();
}

class _BookingDetailsPageState extends State<BookingDetailsPage> {
  final TextEditingController code = TextEditingController();
  String category = "all";
  GoogleMapController _controller;
  DateFormat fDay;

  DateFormat fDate;

  DateFormat fTime;

  @override
  Widget build(BuildContext context) {
    fDay = new DateFormat(
        "EEEE", AppLocalizations.of(context).locale.languageCode);
    fDate = new DateFormat(
        'yyyy-MM-dd', AppLocalizations.of(context).locale.languageCode);
    fTime =
        new DateFormat("jm", AppLocalizations.of(context).locale.languageCode);
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0), // here the desired height
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
              decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.topRight,
                colors: [
                  myColors.orange,
                  myColors.purple100,
                ]),
          )),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 8, right: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: height / 29,
              ),
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Row(
                  children: [
                    Icon(
                      Icons.arrow_back,
                      color: myColors.purple100,
                    ),
                    Text(
                      AppLocalizations.of(context).translate("Event Details"),
                      style: TextStyle(
                          fontSize: 18,
                          color: myColors.purple100,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: height / 29,
              ),
              Center(
                child: QrImage(
                  data: widget.b.id,
                  version: QrVersions.auto,
                  size: 250.0,
                ),
              ),
              SizedBox(
                height: height / 29,
              ),
              InkWell(
                onTap: () {
//                  imageView(e.image);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ImageViewer(widget.b.event.image)));
                },
                child: Center(
                  child: Container(
                    height: 147,
                    width: width - 16,
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: ImageFromNetworkWidget(widget.b.event.image)),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 35, right: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          widget.b.event.category,
                          style: TextStyle(
                              fontSize: 18, color: myColors.purple100),
                        ),
                        Text(
                          widget.b.event.currency +
                              " " +
                              widget.b.event.priceForOneSeat.toString(),
                          style: TextStyle(
                              fontSize: 18, color: myColors.purple100),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      fDay.format(widget.b.event.date) +
                          " " +
                          fDate.format(widget.b.event.date),
                      style: TextStyle(color: myColors.purple100),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      AppLocalizations.of(context).translate("at") +
                          " " +
                          fTime.format(widget.b.event.date),
                      style: TextStyle(color: myColors.purple100),
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          AppLocalizations.of(context).translate("Seats"),
                          style: TextStyle(color: myColors.purple100),
                        ),
                        Text(
                          widget.b.seats.toString() +
                              " " +
                              AppLocalizations.of(context).translate("sit"),
                          style: TextStyle(color: myColors.purple100),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 21,
                    ),
                    Text(
                      AppLocalizations.of(context).translate("Description"),
                      style: TextStyle(fontSize: 18, color: myColors.purple100),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      widget.b.event.description,
                      style: TextStyle(color: myColors.purple100),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      AppLocalizations.of(context).translate("Location"),
                      style: TextStyle(fontSize: 18, color: myColors.purple100),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                  ],
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(color: myColors.purple100, spreadRadius: 1),
                  ],
                ),
                height: 147,
                width: width - 16,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: GoogleMap(
                    onTap: (l) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  MoveCamera(LatLng(
                                      widget.b.event.lat,
                                      widget.b.event.lng))));
                    },
                    mapType: MapType.normal,
                    initialCameraPosition: CameraPosition(
                      target: LatLng(widget.b.event.lat, widget.b.event.lng),
                      zoom: 14.4746,
                    ),
                    onMapCreated: (GoogleMapController controller) {
                      _controller = controller;
                    },
                    zoomControlsEnabled: false,
                  ),
                ),
              ),
              SizedBox(
                height: 8,
              ),
              SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
