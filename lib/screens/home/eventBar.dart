import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:saudi_dates/const/myColors.dart';
import 'package:saudi_dates/customWidget/image_from_network.dart';
import 'package:saudi_dates/language/appLocalizations.dart';
import 'package:saudi_dates/screens/home/bookingDetails.dart';
import 'package:saudi_dates/view_models/home_view_model.dart';

class EventBarPage extends StatefulWidget {
  @override
  _EventBarPageState createState() => _EventBarPageState();
}

class _EventBarPageState extends State<EventBarPage> {
  final TextEditingController code = TextEditingController();
  String category = "all";
  DateFormat fDay;

  DateFormat fDate;

  DateFormat fTime;

  @override
  Widget build(BuildContext context) {
    fDay = new DateFormat(
        "EEEE", AppLocalizations.of(context).locale.languageCode);
    fDate = new DateFormat(
        'yyyy-MM-dd', AppLocalizations.of(context).locale.languageCode);
    fTime =
        new DateFormat("jm", AppLocalizations.of(context).locale.languageCode);
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0), // here the desired height
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
              decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.topRight,
                colors: [
                  myColors.orange,
                  myColors.purple100,
                ]),
          )),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 8, right: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: height / 29,
              ),
              Text(
                AppLocalizations.of(context).translate("My Event"),
                style: TextStyle(
                    fontSize: 18,
                    color: myColors.purple100,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: height / 29,
              ),
              Consumer<HomeViewModel>(builder: (context, home, child) {
                if (home.myEventsLoading)
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                if (home.myBooking.isEmpty)
                  return Padding(
                    padding: EdgeInsets.only(top: height / 4),
                    child: Center(
                        child: Text(
                      AppLocalizations.of(context)
                          .translate("There is no Event"),
                      style: TextStyle(color: Colors.grey, fontSize: 20),
                    )),
                  );
                return Column(
                  children: home.myBooking
                      .map((b) => InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BookingDetailsPage(
                                            b: b,
                                          )));
                            },
                            child: Container(
                              margin: EdgeInsets.only(bottom: 9),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    height: height / 9,
                                    width: width / 2.1,
                                    child: ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(8.0),
                                        child: ImageFromNetworkWidget(
                                            b.event.image)),
                                  ),
                                  SizedBox(
                                    width: width / 25,
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          height: 8,
                                        ),
                                        Text(
                                          b.event.name,
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                              color: myColors.purple100),
                                        ),
                                        SizedBox(
                                          height: 8,
                                        ),
                                        Text(
                                          fDay.format(b.date) +
                                              "\n" +
                                              fDate.format(b.date),
                                          style: TextStyle(
                                              color: myColors.purple100),
                                        ),
                                        Text(
                                          AppLocalizations.of(context)
                                              .translate("at") +
                                              " " +
                                              fTime.format(b.date),
                                          style: TextStyle(
                                              color: myColors.purple100),
                                        ),
                                        SizedBox(
                                          height: 8,
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ))
                      .toList(),
                );
              }),
            ],
          ),
        ),
      ),
    );
  }
}
