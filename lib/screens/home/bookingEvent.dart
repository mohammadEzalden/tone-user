import 'dart:convert';
import 'dart:io';
import 'package:flutter_svg/svg.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:saudi_dates/apis/auth/url.dart' as url;
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_paytabs_bridge/BaseBillingShippingInfo.dart';
import 'package:flutter_paytabs_bridge/PaymentSdkConfigurationDetails.dart';
import 'package:flutter_paytabs_bridge/PaymentSdkLocale.dart';
import 'package:flutter_paytabs_bridge/flutter_paytabs_bridge.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:saudi_dates/apis/payment/tap.dart';
import 'package:saudi_dates/const/myColors.dart';
import 'package:saudi_dates/const/widgets.dart';
import 'package:saudi_dates/language/appLocalizations.dart';
import 'package:saudi_dates/model/event.dart';
import 'package:saudi_dates/model/seat.dart';
import 'package:saudi_dates/view_models/home_view_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BookingEventPage extends StatefulWidget {
  final Event e;

  BookingEventPage({this.e});

  @override
  _BookingEventPageState createState() => _BookingEventPageState();
}

class _BookingEventPageState extends State<BookingEventPage> {
  final TextEditingController code = TextEditingController();
  int count = 1;
  DateFormat fDay;

  Seat seat;
  DateFormat fDate;

  DateFormat fTime;
  var configuration;

  @override
  void initState() {
    seat = widget.e.seats.first;
    configureApp();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    fDay = new DateFormat(
        "EEEE", AppLocalizations.of(context).locale.languageCode);
    fDate = new DateFormat(
        'yyyy-MM-dd', AppLocalizations.of(context).locale.languageCode);
    fTime =
    new DateFormat("jm", AppLocalizations.of(context).locale.languageCode);
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0), // here the desired height
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.topRight,
                    colors: [
                      myColors.orange,
                      myColors.purple100,
                    ]),
              )),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 8, right: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: height / 29,
              ),
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Row(
                  children: [
                    Icon(
                      Icons.arrow_back,
                      color: myColors.purple100,
                    ),
                    Text(
                      AppLocalizations.of(context).translate("Booking Ticket"),
                      style: TextStyle(
                          fontSize: 18,
                          color: myColors.purple100,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: height / 29,
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(color: myColors.purple100, spreadRadius: 1),
                  ],
                ),
                height: height - 220,
                width: width - 16,
                child: Padding(
                  padding: const EdgeInsets.only(left: 17, right: 32),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 17,
                      ),
                      Text(
                        AppLocalizations.of(context).translate("Event"),
                        style: TextStyle(color: myColors.purple100),
                      ),
                      SizedBox(
                        height: 18,
                      ),
                      Text(
                        widget.e.category,
                        style: TextStyle(
                            fontSize: 18,
                            color: myColors.purple100,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 18,
                      ),
                      Text(
                        AppLocalizations.of(context).translate("Location"),
                        style: TextStyle(color: myColors.purple100),
                      ),
                      SizedBox(
                        height: 13,
                      ),
                      Text(
                        widget.e.address,
                        style: TextStyle(color: myColors.purple100),
                      ),
                      SizedBox(
                        height: 21,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                AppLocalizations.of(context).translate("Date"),
                                style: TextStyle(color: myColors.purple100),
                              ),
                              SizedBox(
                                height: 9,
                              ),
                              Text(
                                fDate.format(widget.e.date),
                                style: TextStyle(color: myColors.purple100),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                AppLocalizations.of(context).translate("Time"),
                                style: TextStyle(color: myColors.purple100),
                              ),
                              SizedBox(
                                height: 9,
                              ),
                              Text(
                                fTime.format(widget.e.date),
                                style: TextStyle(color: myColors.purple100),
                              ),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 9,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              children: [
                                Text(
                                  AppLocalizations.of(context)
                                      .translate("Available"),
                                  style: TextStyle(color: myColors.purple100),
                                ),
                                SizedBox(height: 9,),

                                Column(
                                  children: widget.e.seats
                                      .map(
                                        (e) =>
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              bottom: 9),
                                          child: Text(
                                            e.number.toString(),
                                            style:
                                            TextStyle(
                                                color: myColors.purple100),
                                          ),
                                        ),
                                  )
                                      .toList(),
                                ),
                              ],
                            ),
                            Column(
                              children: [
                                Text(
                                  AppLocalizations.of(context).translate(
                                      "Type"),
                                  style: TextStyle(color: myColors.purple100),
                                ),
                                SizedBox(height: 9,),

                                Column(
                                  children: widget.e.seats
                                      .map(
                                        (e) =>
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              bottom: 9),
                                          child: Text(
                                            e.type,
                                            style:
                                            TextStyle(
                                                color: myColors.purple100),
                                          ),
                                        ),
                                  )
                                      .toList(),
                                ),
                              ],
                            ),
                            Column(
                              children: [
                                Text(
                                  AppLocalizations.of(context).translate(
                                      "Price"),
                                  style: TextStyle(color: myColors.purple100),
                                ),
                                SizedBox(height: 9,),
                                Column(
                                  children: widget.e.seats
                                      .map(
                                        (e) =>
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              bottom: 9),
                                          child: Text(
                                            e.price.toString() + " SAR",
                                            style:
                                            TextStyle(
                                                color: myColors.purple100),
                                          ),
                                        ),
                                  )
                                      .toList(),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),

                      Center(
                        child: Text(
                          "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -",
                          style: TextStyle(color: myColors.purple100),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            AppLocalizations.of(context)
                                .translate("Seat type") +
                                " : ",
                            style: TextStyle(color: myColors.purple100),
                          ),
                          SizedBox(
                            width: 7,
                          ),
                          Expanded(
                            child: DropdownButtonFormField<Seat>(
                              value: seat,
                              hint: Text(
                                'Type',
                                style: TextStyle(color: myColors.purple100),
                              ),
                              isExpanded: true,
                              decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.all(0.0),
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide.none,
                                  ),
                                  isDense: true),
                              onChanged: (s) {
                                setState(() {
                                  seat = s;
                                });
                              },
                              validator: (value) =>
                              value == null
                                  ? "   " +
                                  AppLocalizations.of(context)
                                      .translate("Required")
                                  : null,
                              items: widget.e.seats
                                  .map<DropdownMenuItem<Seat>>((Seat value) {
                                return DropdownMenuItem<Seat>(
                                  value: value,
                                  child: Text(
                                    value.type,
                                    style: TextStyle(color: myColors.purple100),
                                  ),
                                );
                              }).toList(),
                            ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Row(
                            children: [
                              InkWell(
                                onTap: () {
                                  if (count > 1) {
                                    count--;
                                    setState(() {});
                                  }
                                },
                                child: Container(
                                  // width: 20,
                                  // height: 20,
                                  child: Center(
                                      child: Padding(
                                        padding: const EdgeInsets.all(3.0),
                                        child: Icon(Icons.remove,
                                            color: myColors.white, size: 15),
                                      )),
                                  decoration: BoxDecoration(
                                      color: myColors.purple100,
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(3)),
                                      border: Border.all(
                                          color: myColors.purple100, width: 1)),
                                ),
                              ),
                              Padding(
                                padding:
                                const EdgeInsets.only(right: 8, left: 8),
                                child: Text("$count"),
                              ),
                              InkWell(
                                onTap: () {
                                  count++;
                                  setState(() {});
                                },
                                child: Container(
                                  // width: 20,
                                  // height: 20,
                                  child: Center(
                                      child: Padding(
                                        padding: const EdgeInsets.all(3.0),
                                        child: Icon(
                                          Icons.add,
                                          color: Colors.white,
                                          size: 15,
                                        ),
                                      )),
                                  decoration: BoxDecoration(
                                      color: myColors.purple100,
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(3)),
                                      border: Border.all(
                                          color: myColors.purple100, width: 1)),
                                ),
                              ),
                              SizedBox(
                                width: 8,
                              ),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: 11),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            AppLocalizations.of(context).translate("Price"),
                            style: TextStyle(color: myColors.purple100),
                          ),
                          Text(
                            widget.e.currency +
                                " " +
                                (seat.price * count).toString(),
                            style: TextStyle(color: myColors.purple100),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Platform.isIOS?
              Container(
                width: width,
                height: 38,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Colors.black,
                        Colors.black,
                      ],
                    ),
                    borderRadius: new BorderRadius.all(Radius.circular(8.0))),
                child: TextButton(
                  onPressed: () {
                    if (seat.number >= count) {
                      bookWithApple(width);
                    } else
                      showToast(
                          AppLocalizations.of(context)
                              .translate("There is no Available sits"),
                          context);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        AppLocalizations.of(context).translate("Book With"),
                        style: TextStyle(color: myColors.white, fontSize: 18),
                      ),
                      SvgPicture.asset("assets/icons/apple.svg",color: Colors.white,),
                      Text("Pay",style: TextStyle(color: myColors.white, fontSize: 18),)
                    ],
                  ),
                ),
              ):Container(),
              Platform.isIOS?

              SizedBox(
                height: 10,
              ):Container(),
              Container(
                width: width,
                height: 38,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Color(0xff904e95),
                        Color(0xffe96443),
                      ],
                    ),
                    borderRadius: new BorderRadius.all(Radius.circular(8.0))),
                child: TextButton(
                  onPressed: () {
                    if (seat.number >= count) {
                      book(width);
                    } else
                      showToast(
                          AppLocalizations.of(context)
                              .translate("There is no Available sits"),
                          context);
                  },
                  child: Text(
                    AppLocalizations.of(context).translate("Booking Ticket"),
                    style: TextStyle(color: myColors.white, fontSize: 18),
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
    );
  }

  book(double width) {
    showDialog(
        context: context,
        builder: (BuildContext cxt) {
          return AlertDialog(
            title: Text(
              AppLocalizations.of(context).translate("Alert"),
              style: TextStyle(
                  color: myColors.purple100, fontWeight: FontWeight.bold),
            ),
            content: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return Container(
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
//                        Text(
//                          AppLocalizations.of(context).translate(
//                              "After completing the purchase, the ticket cannot be returned or exchanged (for more information, please see the terms and conditions)"),
//                          style: TextStyle(
//                              color: myColors.purple100),
//                        ),
//                        Container(
//                          padding: EdgeInsets.only(
//                            bottom: 3, // Space between underline and text
//                          ),
//                          decoration: BoxDecoration(
//                              border: Border(
//                                  bottom: BorderSide(
//                                    color: myColors.orange,
//                                    width: 1.0, // Underline thickness
//                                  ))),
//                          child: InkWell(
//                            onTap: () {
//                              Navigator.pushNamed(context, "/TermsAndConditions");
//                            },
//                            child: Text(
//                              AppLocalizations.of(context)
//                                  .translate("Terms & Conditions")+")",
//                              style: TextStyle(
//                                color: myColors.orange,
//                              ),
//                            ),
//                          ),
//                        ),
                        RichText(
                          text: TextSpan(
                            style: TextStyle(color: Colors.grey),
                            children: <TextSpan>[
                              TextSpan(
                                  text: AppLocalizations.of(context).translate(
                                      "After completing the purchase, the ticket cannot be returned or exchanged (for more information, please see the terms and conditions)")),
                              TextSpan(
                                  text: AppLocalizations.of(context)
                                      .translate("Terms & Conditions"),
                                  style: TextStyle(color: myColors.orange),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      print('Terms of Service"');
                                      Navigator.pushNamed(
                                          context, "/TermsAndConditions");
                                    }),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Container(
                                width: width / 4,
                                padding: EdgeInsets.only(
                                    left: 8, right: 8, top: 12, bottom: 12),
                                child: Center(
                                  child: Text(
                                    AppLocalizations.of(context)
                                        .translate("Discard"),
                                    style: TextStyle(
                                        color: myColors.purple100,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    border:
                                    Border.all(color: myColors.purple100)),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            RawMaterialButton(
                              fillColor: myColors.purple100,
                              splashColor: myColors.purple100,
                              shape: StadiumBorder(
                                  side: BorderSide(color: myColors.purple100)),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: width / 19, vertical: 15.0),
                                child: Text(
                                  AppLocalizations.of(context)
                                      .translate("Book"),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              onPressed: () async {
                                buildShowDialog(context);
                                setupSDKSession();
//                                configureSDK(
//                                    widget.e.id,
//                                    (seat.price * count).toString(),
//                                    context,
//                                    count,
//                                    widget.e.seats.indexOf(seat),
//                                    seat.type);
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0)), //this right here
          );
        });
  }

  bookWithApple(double width) {
    showDialog(
        context: context,
        builder: (BuildContext cxt) {
          return AlertDialog(
            title: Text(
              AppLocalizations.of(context).translate("Alert"),
              style: TextStyle(
                  color: myColors.purple100, fontWeight: FontWeight.bold),
            ),
            content: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return Container(
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
//                        Text(
//                          AppLocalizations.of(context).translate(
//                              "After completing the purchase, the ticket cannot be returned or exchanged (for more information, please see the terms and conditions)"),
//                          style: TextStyle(
//                              color: myColors.purple100),
//                        ),
//                        Container(
//                          padding: EdgeInsets.only(
//                            bottom: 3, // Space between underline and text
//                          ),
//                          decoration: BoxDecoration(
//                              border: Border(
//                                  bottom: BorderSide(
//                                    color: myColors.orange,
//                                    width: 1.0, // Underline thickness
//                                  ))),
//                          child: InkWell(
//                            onTap: () {
//                              Navigator.pushNamed(context, "/TermsAndConditions");
//                            },
//                            child: Text(
//                              AppLocalizations.of(context)
//                                  .translate("Terms & Conditions")+")",
//                              style: TextStyle(
//                                color: myColors.orange,
//                              ),
//                            ),
//                          ),
//                        ),
                        RichText(
                          text: TextSpan(
                            style: TextStyle(color: Colors.grey),
                            children: <TextSpan>[
                              TextSpan(
                                  text: AppLocalizations.of(context).translate(
                                      "After completing the purchase, the ticket cannot be returned or exchanged (for more information, please see the terms and conditions)")),
                              TextSpan(
                                  text: AppLocalizations.of(context)
                                      .translate("Terms & Conditions"),
                                  style: TextStyle(color: myColors.orange),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      print('Terms of Service"');
                                      Navigator.pushNamed(
                                          context, "/TermsAndConditions");
                                    }),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Container(
                                width: width / 4,
                                padding: EdgeInsets.only(
                                    left: 8, right: 8, top: 12, bottom: 12),
                                child: Center(
                                  child: Text(
                                    AppLocalizations.of(context)
                                        .translate("Discard"),
                                    style: TextStyle(
                                        color: myColors.purple100,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    border:
                                    Border.all(color: myColors.purple100)),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            RawMaterialButton(
                              fillColor: myColors.purple100,
                              splashColor: myColors.purple100,
                              shape: StadiumBorder(
                                  side: BorderSide(color: myColors.purple100)),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: width / 19, vertical: 15.0),
                                child: Text(
                                  AppLocalizations.of(context)
                                      .translate("Book"),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              onPressed: () async {
                                buildShowDialog(context);
                                setupSDKSessionApple();
//                                configureSDK(
//                                    widget.e.id,
//                                    (seat.price * count).toString(),
//                                    context,
//                                    count,
//                                    widget.e.seats.indexOf(seat),
//                                    seat.type);
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0)), //this right here
          );
        });
  }

  setupSDKSession() {
    FlutterPaytabsBridge.startCardPayment(configuration, (event) {
      Map<dynamic,dynamic> transactionDetails;
      setState(() {
        if (event["status"] == "success") {
          // Handle transaction details here.
          transactionDetails = event["data"];
          print(transactionDetails);
          if(transactionDetails['paymentResult']['responseStatus'].toString().toLowerCase()=="a"){
            booking(transactionDetails['transactionReference']??"new");
          }
          else{
            showToast(transactionDetails['paymentResult']['responseMessage'], context);
            Navigator.pop(context);
          }
        } else if (event["status"] == "error") {
          Navigator.pop(context);
        } else if (event["status"] == "event") {
          // Handle events here.
        }
      });
    });
  }

  setupSDKSessionApple() {
    FlutterPaytabsBridge.startApplePayPayment(configuration, (event) {
      Map<dynamic,dynamic> transactionDetails;
      setState(() {
        if (event["status"] == "success") {
          // Handle transaction details here.
          transactionDetails = event["data"];
          print(transactionDetails);
          if(transactionDetails['paymentResult']['responseStatus'].toString().toLowerCase()=="a"){
            booking(transactionDetails['transactionReference']??"new");
          }
          else{
            showToast(transactionDetails['paymentResult']['responseMessage'], context);
            Navigator.pop(context);
          }
        } else if (event["status"] == "error") {
          Navigator.pop(context);
        } else if (event["status"] == "event") {
          Navigator.pop(context);
        }
      });
    });
  }

  Future<void> configureApp() async {
    configuration = PaymentSdkConfigurationDetails(
        profileId: "76157",
        serverKey: "SGJNT62NKB-J22WG2ZGNW-RZHDKBZ6RH",
        clientKey: "CHKMK7-PBNR6M-MVTM9T-6K669Q",
        cartId: DateTime.now().toString(),
        cartDescription: "cart desc",
        merchantName: "مؤسسة تون بلس للفعاليات الترفيهية",
        screentTitle: "Pay with Card",
        billingDetails: BillingDetails(
            "billing name",
            "test@mail.com",
            "+966123456789",
            "address line",
            "SA",
            "city",
            "state",
            "zip code"),
        locale: PaymentSdkLocale.DEFAULT,
        //PaymentSdkLocale.AR or PaymentSdkLocale.DEFAULT
        amount: (seat.price * count).toDouble(),
        merchantApplePayIndentifier: "merchant.com.hakaya.tone",
        currencyCode: "sar",
        merchantCountryCode: "sa");
    configuration.showBillingInfo = true;
    configuration.showShippingInfo =false;
  }


  Future<void> booking(String chargeId) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    print(prefs.getString("user"));
    print(prefs.getString("token"));
    print(DateTime.now());
    Map<String, String> header = {
      'Content-Type': 'application/json',
      'x-auth-token': prefs.getString("token")
    };
    var response;
    Uri uri = Uri.parse(url.createBooking());
    response = await http.post(uri,
        body: jsonEncode({
          "charge_id": chargeId,
          "user_id": jsonDecode(prefs.getString("user"))['_id'],
          "event_id": widget.e.id,
          "numberOfSeats": count,
          "seatType": seat.type,
          "index": widget.e.seats.indexOf(seat),
        }),
        headers: header);
    final parsed = await jsonDecode(response.body);

    if (response.statusCode != HttpStatus.ok) {
      showToast(parsed['msg'], context, gravity: ToastGravity.CENTER);
      Navigator.pop(context);
      return;
    } else {
      Provider.of<HomeViewModel>(context, listen: false).getMyBooking();
      Provider.of<HomeViewModel>(context, listen: false).getNotifies();
      showToast("SUCCESS", context, gravity: ToastGravity.CENTER);
      Navigator.popUntil(
        context,
        ModalRoute.withName('/'),
      );
      Navigator.pushNamed(context, '/HomePage');
    }
  }

}
