import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:saudi_dates/const/myColors.dart';
import 'package:saudi_dates/language/appLocalizations.dart';

class TermsAndConditions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0), // here the desired height
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
              decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.topRight,
                colors: [
                  myColors.orange,
                  myColors.purple100,
                ]),
          )),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: [
            SizedBox(
              height: 15,
            ),
            Row(
              children: [
                IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                Text(
                  AppLocalizations.of(context)
                      .translate("Terms and Conditions"),
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Text(
              AppLocalizations.of(context).translate(
                  "The effective date of these terms and conditions is 10/7/2021, as they are modified from time to time on all our services that are provided in Tone App. So your agreement to register in the Tone App means your consent of its terms and conditions. Please read thoroughly the terms and conditions before registering in the Tone App. These terms and conditions apply to all users of the service, including but not limited to individuals, event organizers and any entity that uses the services provided by Tone App."),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              AppLocalizations.of(context).translate("Definitions"),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            Text(
              AppLocalizations.of(context).translate("Definitions1"),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              AppLocalizations.of(context).translate("Our scope of service"),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            Text(
              AppLocalizations.of(context).translate("Our scope of service1"),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              AppLocalizations.of(context).translate(
                  "Registration and creation of a personal account policy"),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            Text(
              AppLocalizations.of(context).translate(
                  "Registration and creation of a personal account policy1"),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              AppLocalizations.of(context).translate("Ticket purchase policy"),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            Text(
              AppLocalizations.of(context).translate("Ticket purchase policy1"),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              AppLocalizations.of(context)
                  .translate("Ticket usage and event attendance policy"),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            Text(
              AppLocalizations.of(context)
                  .translate("Ticket usage and event attendance policy1"),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              AppLocalizations.of(context).translate("Payment Policy"),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            Text(
              AppLocalizations.of(context).translate("Payment Policy1"),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  height: 35,
                  width: 35,
                  child: Image.asset("assets/images/stcpay.png"),
                ),
                Container(
                  height: 47,
                  width: 47,
                  child: Image.asset("assets/images/visa.png"),
                ),
                Container(
                  height: 47,
                  width: 47,
                  child: Image.asset("assets/images/mastercard.png"),
                ),
                SvgPicture.asset(
                  "assets/images/mada.svg",
                  fit: BoxFit.cover,
                  height: 47,
                  width: 47,
                ),
                SvgPicture.asset(
                  "assets/images/Apple_Pay_Mark.svg",
                  height: 30,
                  width: 30,
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              AppLocalizations.of(context).translate("Refund - Return Policy"),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            Text(
              AppLocalizations.of(context).translate("Refund - Return Policy1"),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              AppLocalizations.of(context)
                  .translate("Communication and customer service policy"),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            Text(
              AppLocalizations.of(context)
                  .translate("Communication and customer service policy1"),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              AppLocalizations.of(context).translate("price policy"),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            Text(
              AppLocalizations.of(context).translate("price policy1"),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              AppLocalizations.of(context)
                  .translate("We don't have your credit card information"),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            Text(
              AppLocalizations.of(context)
                  .translate("We don't have your credit card information1"),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              AppLocalizations.of(context).translate("Responsibility"),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            Text(
              AppLocalizations.of(context).translate("Responsibility1"),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              AppLocalizations.of(context).translate("Intellectual property"),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            Text(
              AppLocalizations.of(context).translate("Intellectual property1"),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              AppLocalizations.of(context).translate(
                  "Changes to the Privacy Policy and Data Use Policy"),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            Text(
              AppLocalizations.of(context).translate(
                  "Changes to the Privacy Policy and Data Use Policy1"),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              AppLocalizations.of(context)
                  .translate("Commitments and Warranties"),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            Text(
              AppLocalizations.of(context)
                  .translate("Commitments and Warranties1"),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}
